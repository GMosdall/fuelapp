package com.bithyve.android.fuelapp.menu;

import java.util.List;
import java.util.Locale;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bithyve.android.fuelapp.MainFragmentActivity;
import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.Vehicle;
import com.bithyve.android.fuelapp.data.VehiclesDb;
import com.bithyve.android.fuelapp.preferences.PreferencesFragment;
import com.bithyve.android.fuelapp.vehicles.VehicleStatisticsFragment;
import com.bithyve.android.fuelapp.vehicles.edit.EditVehiclesFragment;

public class MenuListFragment extends ListFragment {

	private static MenuListFragment instance;
	
	private MenuAdapter adapter;
	
	public static MenuListFragment getInstance() {
		return MenuListFragment.instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		MenuListFragment.instance = this;
		return inflater.inflate(R.layout.menu_list, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		updateMenu(getActivity());
	}
	
	public void updateMenu(Context ctx) {
		VehiclesDb db = new VehiclesDb(ctx);
		db.open();
		List<Vehicle> vehicles = db.getAllVehicles();
		db.close();
		
		updateMenu(ctx, vehicles);
	}
	
	public void updateMenu(Context ctx, List<Vehicle> vehicles) {
		adapter = new MenuAdapter(ctx);
		
		if (vehicles.size() > 0) {
			/*
			 * Your vehicles title
			 */
			View vehiclesTitle = getMenuItemView(ctx, R.layout.menu_list_row_title, 0, ctx.getString(R.string.Your_vehicles).toUpperCase(Locale.US), false);
			adapter.add(new MenuItem(vehiclesTitle));
			
			for (Vehicle vehicle : vehicles) {
				/*
				 * Each vehicle
				 */
				View summaryView = getMenuItemView(ctx,
						R.layout.menu_list_row,
						0,
						vehicle.getName(), 
						true);
				adapter.add(new MenuItem(summaryView, FragmentType.VEHICLE_STATISTICS, vehicle.getId()));
			}
			
		}

		/*
		 * Settings title
		 */
		View settingsTitle = getMenuItemView(ctx, R.layout.menu_list_row_title, 0, ctx.getString(R.string.Settings).toUpperCase(Locale.US), false);
		adapter.add(new MenuItem(settingsTitle));
		
		/*
		 * Edit Vehicles
		 */
		View editVehicles = getMenuItemView(ctx,
				R.layout.menu_list_row, 
				0, 
				ctx.getString(R.string.Edit_vehicles), 
				true);
		adapter.add(new MenuItem(editVehicles, FragmentType.EDIT_VEHICLES));
		
		/*
		 * Preferences
		 */
		View preferences = getMenuItemView(ctx,
				R.layout.menu_list_row, 
				0, 
				ctx.getString(R.string.Preferences), 
				true);
		adapter.add(new MenuItem(preferences, FragmentType.PREFERENCES));
		
		setListAdapter(adapter);
	}

	private View getMenuItemView(Context ctx, int layoutId, int iconId, String titleString, boolean clickable) {
//    	LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		View menuItem = inflater.inflate(layoutId, null);
		//This seems to work faster than the possibility above
		View menuItem = LayoutInflater.from(ctx).inflate(layoutId, null);
		
		ImageView icon = (ImageView) menuItem.findViewById(R.id.row_icon);
		if (icon != null && iconId != 0) {
			icon.setImageResource(iconId);
		}
		
		TextView title = (TextView) menuItem.findViewById(R.id.row_title);
		if (title != null && titleString != null) {
			title.setText(titleString);
		}
		
		if (!clickable) {
			menuItem.setOnClickListener(null);
			menuItem.setEnabled(false);
		}
		
		return menuItem;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		MenuItem item = (MenuItem) getListAdapter().getItem(position);
		Fragment fragment = getFragmentFromEnum(item);
		
		if(fragment != null) {
			switchFragment(fragment, item.getVehicleId());
		}
	}

	// the meat of switching the above fragment
	private void switchFragment(Fragment fragment, long vehicleId) {
		if (getActivity() == null)
			return;
		
		if (getActivity() instanceof MainFragmentActivity) {
			MainFragmentActivity main = (MainFragmentActivity) getActivity();
			main.switchContent(fragment, vehicleId);
		}
	}
	
	private Fragment getFragmentFromEnum(MenuItem menuItem) {
		FragmentType fragmentType = menuItem.getFragmentType();
		if (fragmentType == null) {
			return null;
		}
		
		Fragment fragment;
		
		switch (fragmentType) {
		case VEHICLE_STATISTICS:
			fragment = new VehicleStatisticsFragment();
			Bundle bundle = new Bundle();
			bundle.putLong(VehicleStatisticsFragment.VEHICLE_ID, menuItem.getVehicleId());
			fragment.setArguments(bundle);
			break;
			
		case PREFERENCES:
			fragment = new PreferencesFragment();
			break;
			
		case EDIT_VEHICLES:
		default:
			fragment = new EditVehiclesFragment();
			break;
		}
		
		return fragment;
	}

}
