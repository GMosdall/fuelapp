package com.bithyve.android.fuelapp.menu;

import android.view.View;

class MenuItem {
	
	private View view;
	private FragmentType fragmentType;
	private long vehicleId;
	
	MenuItem(View view) {
		this(view, null);
	}
	
	MenuItem(View view, FragmentType fragmentType) {
		this(view, fragmentType, -1);
	}
	
	MenuItem(View view, FragmentType fragmentType, long vehicleId) {
		this.view = view;
		this.fragmentType = fragmentType;
		this.vehicleId = vehicleId;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public FragmentType getFragmentType() {
		return fragmentType;
	}

	public void setFragmentType(FragmentType fragmentType) {
		this.fragmentType = fragmentType;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}
}
