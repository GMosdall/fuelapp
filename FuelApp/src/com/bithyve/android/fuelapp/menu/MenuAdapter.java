package com.bithyve.android.fuelapp.menu;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

class MenuAdapter extends ArrayAdapter<MenuItem> {
	
	MenuAdapter(Context context) {
		super(context, 0);
	}

	public View getView(int position, View rowView, ViewGroup parent) {
		MenuItem item = getItem(position);
		return item.getView();
	}
}
