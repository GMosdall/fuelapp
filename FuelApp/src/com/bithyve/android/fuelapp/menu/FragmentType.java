package com.bithyve.android.fuelapp.menu;

public enum FragmentType {
	VEHICLE_STATISTICS,
	EDIT_VEHICLES,
	PREFERENCES
}
