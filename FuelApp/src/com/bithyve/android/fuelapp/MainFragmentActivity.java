package com.bithyve.android.fuelapp;

import java.util.List;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.Vehicle;
import com.bithyve.android.fuelapp.data.VehiclesDb;
import com.bithyve.android.fuelapp.menu.MenuListFragment;
import com.bithyve.android.fuelapp.utils.PreferencesUtil;
import com.bithyve.android.fuelapp.vehicles.VehicleStatisticsFragment;
import com.bithyve.android.fuelapp.vehicles.edit.EditVehiclesFragment;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

public class MainFragmentActivity extends SlidingFragmentActivity {
	
	private Fragment mContent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState != null) {
			mContent = getFragmentManager().getFragment(savedInstanceState, "content");
		}

		if (mContent == null) {
			mContent = getDefaultFragment();
		}
		
		//Set the actionbar home button
		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		Drawable actionBarBackgroundDrawable = getResources().getDrawable(R.drawable.ab_background_blue);
		actionBar.setBackgroundDrawable(actionBarBackgroundDrawable);
		
		//Set the upper view
		setContentView(R.layout.main_fragment_activity);
		FragmentManager fm = getFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		transaction.replace(R.id.contentFragment, mContent);
		transaction.commit();
		
		//Set the menu
		MenuListFragment menuListFrag = new MenuListFragment();
		setBehindContentView(R.layout.menu_frame);
		FragmentTransaction transactionMenu = this.getFragmentManager().beginTransaction();
		transactionMenu.replace(R.id.menu_frame, menuListFrag);
		transactionMenu.commit();

		//Customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.slidingmenu_shadow_width);
		sm.setShadowDrawable(R.drawable.slidingmenu_shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		
		//Set the preferred units only on the first run
		PreferencesUtil.setPreferredUnitsOnFirstRun(this);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		getFragmentManager().putFragment(outState, "content", mContent);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		if (savedInstanceState != null) {
			mContent = getFragmentManager().getFragment(savedInstanceState, "content");
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean itemSelected;
		
		switch (item.getItemId()) {
		case android.R.id.home:
    		toggle();
			itemSelected = true;
			break;

		default:
			itemSelected = super.onOptionsItemSelected(item);
			break;
		}
		
		return itemSelected;
	}
	
	public void switchContent(Fragment fragment, long newVehicleId) {
		if (fragment == null) {
			fragment = getDefaultFragment();
		}
		
		boolean replaceFragment = false;
		if (!fragment.getClass().equals(mContent.getClass())) {
			replaceFragment = true;
		} else if (fragment instanceof VehicleStatisticsFragment) {
			if (mContent instanceof VehicleStatisticsFragment) {
				/*
				 * VehicleStatisticsFragment can have different content based on
				 * the vehicle that it should show.
				 */
				VehicleStatisticsFragment currentFragment = (VehicleStatisticsFragment) mContent;
				if (newVehicleId != currentFragment.getVehicleId()) {
					replaceFragment = true;
				}
			} else {
				replaceFragment = true;
			}
		}
		
		if (replaceFragment) {
			mContent = fragment;
			FragmentManager fm = getFragmentManager();
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.replace(R.id.contentFragment, fragment);
			transaction.commit();
		}
		
		/*
		 * Added a delay that gives the main thread enough time
		 * to process the actual switching of the fragment 
		 * before the animation is shown.
		 */
		Handler h = new Handler();
		h.postDelayed(new Runnable() {
			public void run() {
				getSlidingMenu().showContent();
			}
		}, 50);
	}
	
	private Fragment getDefaultFragment() {
		Fragment fragment;
		
		VehiclesDb db = new VehiclesDb(this.getApplicationContext());
		db.open();
		List<Vehicle> vehicles = db.getAllVehicles();
		db.close();
		
		if (vehicles.size() > 0) {
			
			Vehicle vehicle = vehicles.get(0);
			Bundle bundle = new Bundle();
			bundle.putLong(VehicleStatisticsFragment.VEHICLE_ID, vehicle.getId());
			
			fragment = new VehicleStatisticsFragment();
			fragment.setArguments(bundle);
			
		} else {
			fragment = new EditVehiclesFragment();
		}
		
		return fragment;
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	
    	switch (keyCode) {
		case KeyEvent.KEYCODE_MENU:
			toggle();
			return true;

		case KeyEvent.KEYCODE_BACK:
			moveTaskToBack(true);
			return true;

		default:
			return super.onKeyDown(keyCode, event);
		}
    }


}
