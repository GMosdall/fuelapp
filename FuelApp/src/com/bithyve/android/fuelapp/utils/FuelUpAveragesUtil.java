package com.bithyve.android.fuelapp.utils;

import com.bithyve.android.fuelapp.data.FuelUpAverages;

public class FuelUpAveragesUtil {

	public static boolean isLastFuelUpBetterThanAverage(FuelUpAverages fuelUpAverages) {
		boolean betterThanAverage = false;
		
		double lastFuelUpConsumption = fuelUpAverages.getLastFuelUpConsumption();
		double averageFuelUpConsumption = fuelUpAverages.getAverageFuelUpConsumption();
		if (lastFuelUpConsumption > averageFuelUpConsumption) {
			betterThanAverage = true;
		}
		
		return betterThanAverage;
	}
}
