package com.bithyve.android.fuelapp.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;

public class DateUtil {

	@SuppressLint("SimpleDateFormat")
	public static String getDayMonthString(Date date) {
		if (date == null) {
			return "";
		}
		
		SimpleDateFormat df = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT); //Will output something like dd/mm/yyyy, so we still need to strip the year
		String pattern = df.toLocalizedPattern().replaceAll(".?[Yy].?", "");
//		System.out.println(pattern);
		SimpleDateFormat dmf = new SimpleDateFormat(pattern);
		String localDate = dmf.format(date);

		return localDate;
	}
	
	@SuppressLint("SimpleDateFormat")
	public static String getMediumDayMonthString(Date date) {
		if (date == null) {
			return "";
		}
		
		SimpleDateFormat df = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.MEDIUM); //Will output something like MMM d, yyyy, so we still need to strip the year
//		DateFormat df = android.text.format.DateFormat.getMediumDateFormat(context);
		String pattern = df.toLocalizedPattern().replaceAll(".?[Yy].?", "").replaceAll(",", "");
		SimpleDateFormat dmf = new SimpleDateFormat(pattern);
		String dateString = dmf.format(date);
		return dateString;
	}
	
	@SuppressLint("SimpleDateFormat")
	public static String getYearString(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		String dateString = df.format(date);
		return dateString;
	}
}
