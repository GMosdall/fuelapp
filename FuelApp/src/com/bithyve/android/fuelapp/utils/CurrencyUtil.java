package com.bithyve.android.fuelapp.utils;

import java.util.Currency;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

import com.bithyve.android.fuelapp.R;

public class CurrencyUtil {
	
	public static String getDisplayName(String currencyCode, Context ctx) {
		String displayName;

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
			Currency currency = Currency.getInstance(currencyCode);
			displayName = getPostKitkatDisplayName(currency);
		} else {
			displayName = getPreKitkatDisplayName(currencyCode, ctx);
		}
		
		return displayName;
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
	private static String getPostKitkatDisplayName(Currency currency) {
		String displayName;
		displayName = currency.getDisplayName();
		return displayName;
	}
	
	private static String getPreKitkatDisplayName(String currencyCode, Context ctx) {
		String displayName;
		
		if (currencyCode.equals("EUR")) {
			displayName = ctx.getString(R.string.Euro);
			
		} else if (currencyCode.equals("GBP")) {
			displayName = ctx.getString(R.string.Pound);
			
		} else if (currencyCode.equals("DKK") || currencyCode.equals("NOK")) {
			displayName = ctx.getString(R.string.Krone);
			
		} else if (currencyCode.equals("RUB")) {
			displayName = ctx.getString(R.string.Ruble);
			
		} else if (currencyCode.equals("USD") || currencyCode.equals("CAD") ||
				currencyCode.equals("AUD") || currencyCode.equals("NZD")) {
			displayName = ctx.getString(R.string.Dollar);
			
		} else if (currencyCode.equals("JPY")) {
			displayName = ctx.getString(R.string.Yen);
			
		} else if (currencyCode.equals("KPW") || currencyCode.equals("KRW")) {
			displayName = ctx.getString(R.string.Won);
			
		} else if (currencyCode.equals("CNY")) {
			displayName = ctx.getString(R.string.Yuan);
			
		} else {
			displayName = currencyCode;
		}
		
		return displayName;
	}
	
	public static String getDisplayNameWithSymbol(String currencyCode, Context ctx) {
		String displayName;
		
		Currency currency = Currency.getInstance(currencyCode);
		displayName = getDisplayNameWithSymbol(currency, ctx);
		
		return displayName;
	}
	
	public static String getDisplayNameWithSymbol(Currency currency, Context ctx) {
		String displayName;
		
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
			displayName = getPostKitkatDisplayName(currency);
		} else {
			displayName = currency.getCurrencyCode();
//			displayName = getPreKitkatDisplayName(currency.getCurrencyCode(), ctx); //Produces unwanted results, like 5 different Dollars (Australian, Canadian, etc.), but they're all shown as just "Dollar".
		}
		
		StringBuilder sb = new StringBuilder(displayName)
			.append(" ").append("(").append(currency.getSymbol()).append(")");
		
		return sb.toString();
	}
}
