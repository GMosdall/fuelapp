package com.bithyve.android.fuelapp.utils;

import java.util.Currency;
import java.util.Locale;

import android.content.Context;

import com.bithyve.android.fuelapp.data.GlobalPreferences;
import com.bithyve.android.fuelapp.preferences.CurrencyUnit;
import com.bithyve.android.fuelapp.preferences.DistanceUnit;
import com.bithyve.android.fuelapp.preferences.EfficiencyUnit;
import com.bithyve.android.fuelapp.preferences.TrackMeterUnit;
import com.bithyve.android.fuelapp.preferences.VolumeUnit;

public class PreferencesUtil {

    
    public static void setPreferredUnitsOnFirstRun(Context ctx) {
    	GlobalPreferences gp = new GlobalPreferences(ctx);
    	boolean isFirstRun = gp.isFirstRun();
    	
    	if (isFirstRun) {
    		detectDefaultPreferences(ctx);
		}
    	
    }

    private static void detectDefaultPreferences(Context ctx) {
    	Locale locale = Locale.getDefault();
//    	Log.d("debug", "locale = " + locale.toString());
    	
    	Currency currency = Currency.getInstance(locale);
    	CurrencyUnit currencyUnit = new CurrencyUnit(currency.getCurrencyCode());
    	
    	//Defaults to using the tripmeter
    	TrackMeterUnit trackMeterUnit = TrackMeterUnit.TRIPMETER;
    	
    	//Using this as a source of Locale code: http://stackoverflow.com/questions/7973023/what-is-the-list-of-supported-languages-locales-on-android
    	final String AU = "en_AU";
    	final String NZ = "en_NZ";
    	
    	VolumeUnit volumeUnit;
    	DistanceUnit distanceUnit;
    	EfficiencyUnit efficiencyUnit;
    	
    	if (locale.equals(Locale.UK)) {
			//UK default settings
    		volumeUnit = VolumeUnit.GALLONS_UK;
    		distanceUnit = DistanceUnit.MILES;
    		efficiencyUnit = EfficiencyUnit.MILES_PER_GALLON_UK;
			
		} else if (locale.equals(Locale.US)) {
			//US default settings
			volumeUnit = VolumeUnit.GALLONS_US;
			distanceUnit = DistanceUnit.MILES;
			efficiencyUnit = EfficiencyUnit.MILES_PER_GALLON_US;
    		
		} else if (locale.equals(Locale.CANADA) || locale.equals(Locale.CANADA_FRENCH) ||
				locale.equals(AU) || locale.equals(NZ)) {
			//L/100km
			volumeUnit = VolumeUnit.LITRES;
			distanceUnit = DistanceUnit.KILOMETRES;
			efficiencyUnit = EfficiencyUnit.LITRE_PER_HUNDRED_KM;
			
		} else {
			//km/L
			volumeUnit = VolumeUnit.LITRES;
			distanceUnit = DistanceUnit.KILOMETRES;
			efficiencyUnit = EfficiencyUnit.KM_PER_LITRE;
		}

    	GlobalPreferences gp = new GlobalPreferences(ctx);
    	gp.putCurrencyUnit(currencyUnit);
    	gp.putVolumeUnit(volumeUnit);
    	gp.putTrackMeterUnit(trackMeterUnit);
    	gp.putDistanceUnit(distanceUnit);
    	gp.putEfficiencyUnit(efficiencyUnit);
    	gp.putIsFirstRun(false);
    	gp.apply();
    }
}
