package com.bithyve.android.fuelapp.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bithyve.android.fuelapp.R;

public class EditRow extends LinearLayout {
	
	private String mTitleText;
	private String mContentText;
	private String mHintText;
	private int mInputType;

	public EditRow(Context context) {
		super(context);
	}

	public EditRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray a = context.getTheme().obtainStyledAttributes(
			attrs,
			R.styleable.EditRow,
			0, 0);
		
		try {
			mTitleText = a.getString(R.styleable.EditRow_titleText);
			mContentText = a.getString(R.styleable.EditRow_contentText);
			mHintText = a.getString(R.styleable.EditRow_hintText);
			mInputType = a.getInteger(R.styleable.EditRow_inputType, 0);
		} finally {
			a.recycle();
		}
		
		View view = LayoutInflater.from(context).inflate(R.layout.edit_row, this, true);
		if (mTitleText != null && mTitleText.length() > 0) {
			TextView titleView = (TextView) view.findViewById(R.id.titleView);
			titleView.setText(mTitleText);
		}

		EditText contentView = (EditText) view.findViewById(R.id.contentView);
		if (mContentText != null && mContentText.length() > 0) {
			contentView.setText(mContentText);
		}
		
		if (mHintText != null && mHintText.length() > 0) {
			contentView.setHint(mHintText);
		}
		
		contentView.setInputType(mInputType);
		
	}

	public EditRow(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

}
