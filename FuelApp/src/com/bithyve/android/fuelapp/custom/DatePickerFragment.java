package com.bithyve.android.fuelapp.custom;

import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

public class DatePickerFragment extends DialogFragment {
	
	public static final String DATE = "date";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	Calendar cal = null;
    	
    	Bundle bundle = getArguments();
		if (bundle != null) {
	    	//See if there's a given date
			Date date = (Date) bundle.getSerializable(DATE);
			if (date != null) {
				cal = Calendar.getInstance();
				cal.setTime(date);
			}
		}
		
		if(cal == null) {
	        // Use the current date as the default date in the picker
			cal = Calendar.getInstance();
		}
		
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
    	

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), (OnDateSetListener)getActivity(), year, month, day);
    }

}