package com.bithyve.android.fuelapp.data;

import com.bithyve.android.fuelapp.preferences.DistanceUnit;
import com.bithyve.android.fuelapp.preferences.EfficiencyUnit;
import com.bithyve.android.fuelapp.preferences.TrackMeterUnit;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class Vehicle {

	private long id;
	private String name;
	private String make;
	private String model;
	private int year;
	private String fuelType;
	private String notes;
	private Drawable image;
	
	//Preferences settings
	private TrackMeterUnit trackMeterUnit;
	private DistanceUnit distanceUnit;
	private EfficiencyUnit efficiencyUnit;
	
	private TrackMeterUnit displayTrackMeterUnit;
	private DistanceUnit displayDistanceUnit;
	private EfficiencyUnit displayEfficiencyUnit;
	
	public Vehicle(String name) {
		this.name = name;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getMake() {
		return make;
	}
	
	public void setMake(String make) {
		this.make = make;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public String getFuelType() {
		return fuelType;
	}
	
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	
	public String getNotes() {
		return notes;
	}
	
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public Drawable getImage() {
		return image;
	}
	
	public void setImage(Drawable image) {
		this.image = image;
	}
	
	public TrackMeterUnit getDisplayTrackMeterUnit(Context ctx) {
		if (displayTrackMeterUnit == null || trackMeterUnit == null) {
			GlobalPreferences gb = new GlobalPreferences(ctx);
			displayTrackMeterUnit = gb.getTrackMeterUnit();
			trackMeterUnit = gb.getTrackMeterUnit();
		}
		
		return displayTrackMeterUnit;
	}
	
	public TrackMeterUnit getStorageTrackMeterUnit() throws NullPointerException {
		if (trackMeterUnit == null) {
			throw new NullPointerException();
		}
		
		return trackMeterUnit;
	}
	
	public void setTrackMeterUnit(TrackMeterUnit trackMeterUnit) {
		this.displayTrackMeterUnit = trackMeterUnit;
		this.trackMeterUnit = trackMeterUnit;
	}
	
	public DistanceUnit getDisplayDistanceUnit(Context ctx) {
		if (displayDistanceUnit == null || distanceUnit == null) {
			GlobalPreferences gb = new GlobalPreferences(ctx);
			displayDistanceUnit = gb.getDistanceUnit();
			distanceUnit = gb.getDistanceUnit();
		}
		
		return displayDistanceUnit;
	}
	public DistanceUnit getStorageDistanceUnit() throws NullPointerException {
		if (distanceUnit == null) {
			throw new NullPointerException();
		}
		
		return distanceUnit;
	}
	
	public void setDistanceUnit(DistanceUnit distanceUnit) {
		this.displayDistanceUnit = distanceUnit;
		this.distanceUnit = distanceUnit;
	}

	public EfficiencyUnit getDisplayEfficiencyUnit(Context ctx) {
		if (displayEfficiencyUnit == null || efficiencyUnit == null) {
			GlobalPreferences gb = new GlobalPreferences(ctx);
			displayEfficiencyUnit = gb.getEfficiencyUnit();
			efficiencyUnit = gb.getEfficiencyUnit();
		}
		
		return displayEfficiencyUnit;
	}
	
	public EfficiencyUnit getStorageEfficiencyUnit() throws NullPointerException {
		if (efficiencyUnit == null) {
			throw new NullPointerException();
		}
		
		return efficiencyUnit;
	}
	
	public void setEfficiencyUnit(EfficiencyUnit efficiencyUnit) {
		this.displayEfficiencyUnit = efficiencyUnit;
		this.efficiencyUnit = efficiencyUnit;
	}
	
}
