package com.bithyve.android.fuelapp.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class FuelUpsDb {
	
	private final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm";
	
	private DbHelper dbHelper;
	private SQLiteDatabase db;
	
	public FuelUpsDb(Context ctx) {
		dbHelper = new DbHelper(ctx);
	}
	
	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public long storeFuelUp(FuelUp fuelUp, FuelUp oldFuelUp, long previousFuelUpId) {
		
		ContentValues values = new ContentValues();
		values.put(DbHelper.FUEL_UP_VEHICLE_ID, fuelUp.getVehicleId());
		values.put(DbHelper.FUEL_UP_DISTANCE, fuelUp.getDistance());
		values.put(DbHelper.FUEL_UP_AMOUNT, fuelUp.getAmount());
		values.put(DbHelper.FUEL_UP_PRICE, fuelUp.getPrice());
		values.put(DbHelper.FUEL_UP_NOTES, fuelUp.getNotes());
		
		String dateString = dateToString(fuelUp.getDate());
		values.put(DbHelper.FUEL_UP_DATE, dateString);
		
		long rowID = -1;
		FuelUpAveragesDb averagesDb = new FuelUpAveragesDb(db);
		if (fuelUp.getId() > 0) {
			//Fuel up already exists! Replace with current data
			values.put(DbHelper.FUEL_UP_ID, fuelUp.getId());
			rowID = db.replace(DbHelper.FUEL_UPS_TABLE_NAME, null, values);
			boolean isMostRecentFuelUp = previousFuelUpId > 0 ? true : false;
			averagesDb.replaceFuelUp(fuelUp, oldFuelUp, isMostRecentFuelUp);
		} else {
			//A new fuel up, insert as new data
			rowID = db.insert(DbHelper.FUEL_UPS_TABLE_NAME, null, values);
			averagesDb.addFuelUp(fuelUp);
		}
		
		return rowID;
	}
	
	public FuelUp getFuelUpById(long id) {
		FuelUp fuelUp = null;
		if (id <= 0) {
			return fuelUp;
		}
		
		Cursor cursor = db.query(
				DbHelper.FUEL_UPS_TABLE_NAME, 
				null, //(Returning) Columns
    			DbHelper.FUEL_UP_ID + " = " + id, //Selection (WHERE)
    			null, //SelectionArgs (You may include ?s in selection, which will be replaced by the values from selectionArgs)
    			null, //GroupBy
    			null, //Having
    			null, //OrderBy
				"1" //Limit
				);
		cursor.moveToFirst();
		fuelUp = cursorToFuelUp(cursor);
	    cursor.close();
		
		return fuelUp;
	}
	
	public List<FuelUp> getAllFuelUpsByVehicle(long vehicleId) {
		List<FuelUp> list = new ArrayList<FuelUp>();
		
	    Cursor cursor = db.query(
				DbHelper.FUEL_UPS_TABLE_NAME, 
				null, //(Returning) Columns
    			DbHelper.FUEL_UP_VEHICLE_ID + " = " + vehicleId, //Selection (WHERE)
    			null, //SelectionArgs (You may include ?s in selection, which will be replaced by the values from selectionArgs)
    			null, //GroupBy
    			null, //Having
    			null, //OrderBy
				null //Limit
				);
	    
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	FuelUp fuelUp = cursorToFuelUp(cursor);
	    	list.add(fuelUp);
	    	cursor.moveToNext();
	    }
	    cursor.close();
	    
		return list;
	}
	
	private FuelUp cursorToFuelUp(Cursor cursor) {
		FuelUp fuelUp = null;
		if (cursor == null || cursor.isBeforeFirst() || cursor.isAfterLast() || cursor.isClosed()) {
			return fuelUp;
		}

		fuelUp = new FuelUp(cursor.getLong(cursor.getColumnIndex(DbHelper.FUEL_UP_VEHICLE_ID)));
		fuelUp.setId(cursor.getLong(cursor.getColumnIndex(DbHelper.FUEL_UP_ID)));
		fuelUp.setDistance(cursor.getDouble(cursor.getColumnIndex(DbHelper.FUEL_UP_DISTANCE)));
		fuelUp.setAmount(cursor.getDouble(cursor.getColumnIndex(DbHelper.FUEL_UP_AMOUNT)));
		fuelUp.setPrice(cursor.getDouble(cursor.getColumnIndex(DbHelper.FUEL_UP_PRICE)));
		fuelUp.setNotes(cursor.getString(cursor.getColumnIndex(DbHelper.FUEL_UP_NOTES)));
		
		String dateString = cursor.getString(cursor.getColumnIndex(DbHelper.FUEL_UP_DATE));
		try {
			Date date = stringToDate(dateString);
			fuelUp.setDate(date);
		} catch (ParseException e) {
			Log.w("FuelApp", "Error parsing date from SQLite");
			e.printStackTrace();
		}
		
		return fuelUp;
	}
	
	private SimpleDateFormat getDateFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_FORMAT, Locale.US);
//		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		return sdf;
	}
	
	private String dateToString(Date date) {
		String dateString = null;
		if (date == null) {
			return dateString;
		}
		
		SimpleDateFormat sdf = getDateFormat();
		dateString = sdf.format(date);
		
		return dateString;
	}
	
	private Date stringToDate(String dateString) throws ParseException {
		Date date = null;
		if (dateString == null || dateString.length() < 1) {
			return date;
		}
		
		SimpleDateFormat sdf = getDateFormat();
		date = sdf.parse(dateString);
		
		return date;
	}

}