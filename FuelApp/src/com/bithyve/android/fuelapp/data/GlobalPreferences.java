package com.bithyve.android.fuelapp.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.bithyve.android.fuelapp.preferences.CurrencyUnit;
import com.bithyve.android.fuelapp.preferences.DistanceUnit;
import com.bithyve.android.fuelapp.preferences.EfficiencyUnit;
import com.bithyve.android.fuelapp.preferences.TrackMeterUnit;
import com.bithyve.android.fuelapp.preferences.VolumeUnit;

public class GlobalPreferences {

	private final String GLOBAL_PREFERENCES = "globalPreferences";
	
	private final String IS_FIRST_RUN = "isFirstRun";

	private final String CURRENCY_UNIT = "currencyUnit";
	private final String VOLUME_UNIT = "volumeUnit";
	
	private final String TRACK_METER_UNIT = "trackMeterUnit";
	private final String DISTANCE_UNIT = "distanceUnit";
	private final String EFFICIENCY_UNIT = "efficiencyUnit";

	private SharedPreferences sp;
	private SharedPreferences.Editor spEditor;
	
	public GlobalPreferences(Context ctx) {
		sp = ctx.getSharedPreferences(GLOBAL_PREFERENCES, Context.MODE_PRIVATE);
	}
	
	/*
	 * String setters & getters
	 */
	
	private void putString(String key, String value) {
		if (spEditor == null) {
			spEditor = sp.edit();
		}
		
		spEditor.putString(key, value);
	}
    
	private String getString(String key) {
        return getString(key, "");
    }
    
	private String getString(String key, String defaultValue) {
        return sp.getString(key, defaultValue);
    }
    
	/*
	 * Int setters & getters
	 */
	
	private void putInt(String key, int value) {
		if (spEditor == null) {
			spEditor = sp.edit();
		}
		
        spEditor.putInt(key, value);
    }
    
	private int getInt(String key) {
        return getInt(key, -1);
    }
    
	private int getInt(String key, int defaultValue) {
        return sp.getInt(key, defaultValue);
    }
    
    /*
     * Boolean setters & getters
     */
    
	private void putBoolean(String key, boolean value) {
		if (spEditor == null) {
			spEditor = sp.edit();
		}
		
        spEditor.putBoolean(key, value);
    }
    
	/*private boolean getBoolean(String key) {
        return getBoolean(key, false);
    }*/
    
	private boolean getBoolean(String key, boolean defaultValue) {
        return sp.getBoolean(key, defaultValue);
    }
    
    /*
     * Misc SharedPreferences methods
     */
    
    public void apply() {
    	if (spEditor != null) {
			spEditor.apply();
		}
    }
    
	//Not necessary just yet, but it's nice as a reminder that it's possible
	/*public void remove(String key) {
		if (spEditor == null) {
			spEditor = sp.edit();
		}
		
        spEditor.remove(key);
        spEditor.commit();
    }
    
	public void clear() {
		if (spEditor == null) {
			spEditor = sp.edit();
		}
		
        spEditor.clear();
        spEditor.commit();
    }*/
    
    
    /*
     * Specific convenience methods
     */
    
    public void putIsFirstRun(boolean isFirstRun) {
    	putBoolean(IS_FIRST_RUN, isFirstRun);
    }
    
    public boolean isFirstRun() {
    	boolean isFirstRun;
    	isFirstRun = getBoolean(IS_FIRST_RUN, true);
    	
    	return isFirstRun;
    }
    
    public void putCurrencyUnit(CurrencyUnit currency) {
    	String currencyCode = currency.getCurrencyCode();
    	putString(CURRENCY_UNIT, currencyCode);
    }
    
    public CurrencyUnit getCurrencyUnit() {
    	CurrencyUnit currency;
    	String currencyCode = getString(CURRENCY_UNIT);
    	currency = new CurrencyUnit(currencyCode);
    	
    	return currency;
    }
    
    public void putVolumeUnit(VolumeUnit volume) {
    	int volumeId = volume.getId();
    	putInt(VOLUME_UNIT, volumeId);
    }
    
    public VolumeUnit getVolumeUnit() {
    	VolumeUnit volume;
    	int volumeId = getInt(VOLUME_UNIT);
    	volume = VolumeUnit.newInstance(volumeId);
    	
    	return volume;
    }
    
    public void putTrackMeterUnit(TrackMeterUnit trackMeter) {
    	int trackMeterId = trackMeter.getId();
    	putInt(TRACK_METER_UNIT, trackMeterId);
    }
    
    public TrackMeterUnit getTrackMeterUnit() {
    	TrackMeterUnit trackMeter;
    	int trackMeterId = getInt(TRACK_METER_UNIT);
    	trackMeter = TrackMeterUnit.newInstance(trackMeterId);
    	
    	return trackMeter;
    }
    
    public void putDistanceUnit(DistanceUnit distance) {
    	int distanceId = distance.getId();
    	putInt(DISTANCE_UNIT, distanceId);
    }
    
    public DistanceUnit getDistanceUnit() {
    	DistanceUnit distance;
    	int distanceId = getInt(DISTANCE_UNIT);
    	distance = DistanceUnit.newInstance(distanceId);
    	
    	return distance;
    }
    
    public void putEfficiencyUnit(EfficiencyUnit efficiency) {
    	int efficiencyId = efficiency.getId();
    	putInt(EFFICIENCY_UNIT, efficiencyId);
    }
    
    public EfficiencyUnit getEfficiencyUnit() {
    	EfficiencyUnit efficiency;
    	int efficiencyId = getInt(EFFICIENCY_UNIT);
    	efficiency = EfficiencyUnit.newInstance(efficiencyId);
    	
    	return efficiency;
    }
}
