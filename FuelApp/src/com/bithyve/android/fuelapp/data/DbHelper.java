package com.bithyve.android.fuelapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {

	//Vehicle column names
	public static final String VEHICLE_ID = "_id";
	public static final String VEHICLE_NAME = "name";
	public static final String VEHICLE_MAKE = "make";
	public static final String VEHICLE_MODEL = "model";
	public static final String VEHICLE_YEAR = "year";
	public static final String VEHICLE_FUEL_TYPE = "fuelType";
	public static final String VEHICLE_NOTES = "notes";
	public static final String VEHICLE_IMAGE = "image";
	public static final String VEHICLE_TRACK_METER_UNIT = "trackMeterUnit";
	public static final String VEHICLE_DISTANCE_UNIT = "distanceUnit";
	public static final String VEHICLE_EFFICIENCY_UNIT = "efficiencyUnit";
	
	public static final String[] VEHICLE_ALL_COLUMNS = {
		VEHICLE_ID, VEHICLE_NAME, VEHICLE_MAKE, VEHICLE_MODEL, VEHICLE_YEAR, VEHICLE_FUEL_TYPE, VEHICLE_NOTES, VEHICLE_IMAGE,
		VEHICLE_TRACK_METER_UNIT, VEHICLE_DISTANCE_UNIT, VEHICLE_EFFICIENCY_UNIT};

	//Fuel up column names
	public static final String FUEL_UP_ID = "_id";
	public static final String FUEL_UP_VEHICLE_ID = "vehicleId";
	public static final String FUEL_UP_DISTANCE = "distance";
	public static final String FUEL_UP_AMOUNT = "amount";
	public static final String FUEL_UP_PRICE = "price";
	public static final String FUEL_UP_DATE = "date";
	public static final String FUEL_UP_NOTES = "notes";
	public static final String FUEL_CURRENCY_UNIT = "currencyUnit";
	public static final String FUEL_VOLUME_UNIT = "volumeUnit";
	
	public static final String[] FUEL_UP_ALL_COLUMNS = {
		FUEL_UP_ID, FUEL_UP_VEHICLE_ID, FUEL_UP_DISTANCE, FUEL_UP_AMOUNT, FUEL_UP_PRICE, FUEL_UP_DATE, FUEL_UP_NOTES,
		FUEL_CURRENCY_UNIT, FUEL_VOLUME_UNIT};

	//Fuel up averages column names
	public static final String FUEL_UP_AVERAGES_ID = "_id";
	public static final String FUEL_UP_AVERAGES_VEHICLE_ID = "vehicleId";
	public static final String FUEL_UP_AVERAGES_LAST_FUEL_UP_CONSUMPTION = "lastFuelUpConsumption";
	public static final String FUEL_UP_AVERAGES_LAST_FUEL_UP_PRICE = "lastFuelUpPrice";
	public static final String FUEL_UP_AVERAGES_TOTAL_DISTANCE = "totalDistance";
	public static final String FUEL_UP_AVERAGES_TOTAL_FUEL_CONSUMED = "totalFuelConsumed";
	public static final String FUEL_UP_AVERAGES_TOTAL_MONEY_SPENT = "totalMoneySpent";
	public static final String FUEL_UP_AVERAGES_TOTAL_FUEL_UPS = "totalFuelUps";
	
	public static final String[] FUEL_UP_AVERAGES_ALL_COLUMNS = {
		FUEL_UP_AVERAGES_ID, FUEL_UP_AVERAGES_VEHICLE_ID, FUEL_UP_AVERAGES_LAST_FUEL_UP_CONSUMPTION, 
		FUEL_UP_AVERAGES_LAST_FUEL_UP_PRICE, FUEL_UP_AVERAGES_TOTAL_DISTANCE, FUEL_UP_AVERAGES_TOTAL_FUEL_CONSUMED, 
		FUEL_UP_AVERAGES_TOTAL_MONEY_SPENT, FUEL_UP_AVERAGES_TOTAL_FUEL_UPS};
	
	public static final String VEHICLES_TABLE_NAME = "vehicles";
	public static final String FUEL_UPS_TABLE_NAME = "fuelUps";
	public static final String FUEL_UP_AVERAGES_TABLE_NAME = "fuelUpAverages";
	
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "fuelApp.db";
	
	private static final String CREATE_TABLE_VEHICLES =
			"CREATE TABLE " + VEHICLES_TABLE_NAME + " (" +
			VEHICLE_ID + " integer primary key autoincrement, " +
			VEHICLE_NAME + " TEXT, " +
			VEHICLE_MAKE + " TEXT, " +
			VEHICLE_MODEL + " TEXT, " +
			VEHICLE_YEAR + " INTEGER, " +
			VEHICLE_FUEL_TYPE + " TEXT, " +
			VEHICLE_NOTES + " TEXT, " +
			VEHICLE_IMAGE + " BLOB, " +
			VEHICLE_TRACK_METER_UNIT + " INTEGER, " +
			VEHICLE_DISTANCE_UNIT + " INTEGER, " +
			VEHICLE_EFFICIENCY_UNIT + " INTEGER" +
			");";
	
	private static final String CREATE_TABLE_FUEL_UPS =
			"CREATE TABLE " + FUEL_UPS_TABLE_NAME + " (" +
			FUEL_UP_ID + " integer primary key autoincrement, " +
			FUEL_UP_VEHICLE_ID + " INTEGER, " +
			FUEL_UP_DISTANCE + " DOUBLE, " +
			FUEL_UP_AMOUNT + " DOUBLE, " +
			FUEL_UP_PRICE + " DOUBLE, " +
			FUEL_UP_DATE + " TEXT, " +
			FUEL_UP_NOTES + " TEXT, " +
			FUEL_CURRENCY_UNIT + " INTEGER, " +
			FUEL_VOLUME_UNIT + " INTEGER" +
			"); ";
	
	private static final String CREATE_TABLE_FUEL_UP_AVERAGES =
			"CREATE TABLE " + FUEL_UP_AVERAGES_TABLE_NAME + " (" +
			FUEL_UP_AVERAGES_ID + " integer primary key autoincrement, " +
			FUEL_UP_AVERAGES_VEHICLE_ID + " INTEGER, " +
			FUEL_UP_AVERAGES_LAST_FUEL_UP_CONSUMPTION + " DOUBLE, " +
			FUEL_UP_AVERAGES_LAST_FUEL_UP_PRICE + " DOUBLE, " +
			FUEL_UP_AVERAGES_TOTAL_DISTANCE + " DOUBLE, " +
			FUEL_UP_AVERAGES_TOTAL_FUEL_CONSUMED + " DOUBLE, " +
			FUEL_UP_AVERAGES_TOTAL_MONEY_SPENT + " DOUBLE, " +
			FUEL_UP_AVERAGES_TOTAL_FUEL_UPS + " INTEGER" +
			"); ";

	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_VEHICLES);
		db.execSQL(CREATE_TABLE_FUEL_UPS);
		db.execSQL(CREATE_TABLE_FUEL_UP_AVERAGES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO: Implement the upgrade when needed
		Log.w("debug", "Nothing to upgrade yet...");
	}

}
