package com.bithyve.android.fuelapp.data;

import java.util.Date;

import android.content.Context;

import com.bithyve.android.fuelapp.preferences.CurrencyUnit;
import com.bithyve.android.fuelapp.preferences.DistanceUnit;
import com.bithyve.android.fuelapp.preferences.VolumeUnit;

public class FuelUp {

	private long id;
	private long vehicleId;
	private double distance;
	private double amount;
	private double price;
	private Date date;
	private String notes;
	
	//Preferences settings
	private CurrencyUnit currencyUnit;
	private VolumeUnit volumeUnit;
	private DistanceUnit distanceUnit;
	
	private CurrencyUnit displayCurrencyUnit;
	private VolumeUnit displayVolumeUnit;
	private DistanceUnit displayDistanceUnit;
	
	public FuelUp(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public CurrencyUnit getDisplayCurrencyUnit(Context ctx) {
		if (displayCurrencyUnit == null || currencyUnit == null) {
			GlobalPreferences gb = new GlobalPreferences(ctx);
			displayCurrencyUnit = gb.getCurrencyUnit();
			currencyUnit = gb.getCurrencyUnit();
		}
		
		return currencyUnit;
	}

	public CurrencyUnit getStorageCurrencyUnit() throws NullPointerException {
		if (currencyUnit == null) {
			throw new NullPointerException();
		}
		
		return currencyUnit;
	}

	public void setCurrencyUnit(CurrencyUnit currencyUnit) {
		this.displayCurrencyUnit = currencyUnit;
		this.currencyUnit = currencyUnit;
	}

	public VolumeUnit getDisplayVolumeUnit(Context ctx) {
		if (displayVolumeUnit == null || volumeUnit == null) {
			GlobalPreferences gb = new GlobalPreferences(ctx);
			displayVolumeUnit = gb.getVolumeUnit();
			volumeUnit = gb.getVolumeUnit();
		}
		
		return displayVolumeUnit;
	}

	public VolumeUnit getStorageVolumeUnit() throws NullPointerException {
		if (volumeUnit == null) {
			throw new NullPointerException();
		}
		
		return volumeUnit;
	}

	public void setVolumeUnit(VolumeUnit volumeUnit) {
		this.displayVolumeUnit = volumeUnit;
		this.volumeUnit = volumeUnit;
	}
	
	public DistanceUnit getDisplayDistanceUnit(Context ctx) {
		if (displayDistanceUnit == null || distanceUnit == null) {
			GlobalPreferences gb = new GlobalPreferences(ctx);
			displayDistanceUnit = gb.getDistanceUnit();
			distanceUnit = gb.getDistanceUnit();
		}
		
		return displayDistanceUnit;
	}
	public DistanceUnit getStorageDistanceUnit() throws NullPointerException {
		if (distanceUnit == null) {
			throw new NullPointerException();
		}
		
		return distanceUnit;
	}
	
	public void setDistanceUnit(DistanceUnit distanceUnit) {
		this.displayDistanceUnit = distanceUnit;
		this.distanceUnit = distanceUnit;
	}
	
}
