package com.bithyve.android.fuelapp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class FuelUpAveragesDb {

	private DbHelper dbHelper;
	private SQLiteDatabase db;
	
	FuelUpAveragesDb(SQLiteDatabase db) {
		this.db = db;
	}
	
	public FuelUpAveragesDb(Context ctx) {
		dbHelper = new DbHelper(ctx);
	}
	
	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	long addFuelUp(FuelUp fuelUp) {
		if (fuelUp == null) {
			//Crap in is crap out
			return -1;
		}
		
		boolean replaceAverages = true;
		FuelUpAverages fuelUpAverages = getFuelUpAverageByVehicleId(fuelUp.getVehicleId());
		if (fuelUpAverages == null) {
			fuelUpAverages = new FuelUpAverages(fuelUp.getVehicleId());
			replaceAverages = false;
		}
		fuelUpAverages.setLastFuelUpConsumption(fuelUp.getDistance() / fuelUp.getAmount());
		fuelUpAverages.setLastFuelUpPrice(fuelUp.getPrice() * fuelUp.getAmount());
		fuelUpAverages.setTotalDistance(fuelUpAverages.getTotalDistance() + fuelUp.getDistance());
		fuelUpAverages.setTotalFuelConsumed(fuelUpAverages.getTotalFuelConsumed() + fuelUp.getAmount());
		fuelUpAverages.setTotalMoneySpent(fuelUpAverages.getTotalMoneySpent() + fuelUpAverages.getLastFuelUpPrice());
		fuelUpAverages.setTotalFuelUps(fuelUpAverages.getTotalFuelUps() + 1);
		
		long rowID = storeFuelUpAverage(fuelUpAverages, replaceAverages);
		
		return rowID;
	}
	
	long replaceFuelUp(FuelUp newFuelUp, FuelUp oldFuelUp, boolean isMostRecentFuelUp) {
		if (newFuelUp == null || oldFuelUp == null) {
			//Crap in is crap out
			return -1;
		}
		
		FuelUpAverages fuelUpAverages = getFuelUpAverageByVehicleId(newFuelUp.getVehicleId());
		if (fuelUpAverages == null) {
			//This is impossibru!
			return -1;
		}
		
		if (isMostRecentFuelUp) {
			fuelUpAverages.setLastFuelUpConsumption(newFuelUp.getDistance() / newFuelUp.getAmount());
			fuelUpAverages.setLastFuelUpPrice(newFuelUp.getPrice() * newFuelUp.getAmount());
		}
		
		double totalDistance = fuelUpAverages.getTotalDistance() - oldFuelUp.getDistance();
		fuelUpAverages.setTotalDistance(totalDistance + newFuelUp.getDistance());
		double fuelConsumed = fuelUpAverages.getTotalFuelConsumed() - oldFuelUp.getAmount();
		fuelUpAverages.setTotalFuelConsumed(fuelConsumed + newFuelUp.getAmount());
		double moneySpent = fuelUpAverages.getTotalMoneySpent() - (oldFuelUp.getPrice() * oldFuelUp.getAmount());
		fuelUpAverages.setTotalMoneySpent(moneySpent + (newFuelUp.getPrice() * newFuelUp.getAmount()));

		long rowID = storeFuelUpAverage(fuelUpAverages, true);
		
		return rowID;
	}
	
	long removeFuelUp(FuelUp fuelUp, FuelUp previousFuelUp) {
		if (fuelUp == null) {
			//Crap in is crap out
			return -1;
		}
		
		FuelUpAverages fuelUpAverages = getFuelUpAverageByVehicleId(fuelUp.getVehicleId());
		if (fuelUpAverages == null) {
			//This is impossibru!
			return -1;
		}
		if (previousFuelUp != null) {
			fuelUpAverages.setLastFuelUpConsumption(previousFuelUp.getDistance() / previousFuelUp.getAmount());
			fuelUpAverages.setLastFuelUpPrice(previousFuelUp.getPrice() * previousFuelUp.getAmount());
		}
		
		fuelUpAverages.setTotalDistance(fuelUpAverages.getTotalDistance() - fuelUp.getDistance());
		fuelUpAverages.setTotalFuelConsumed(fuelUpAverages.getTotalFuelConsumed() - fuelUp.getAmount());
		fuelUpAverages.setTotalMoneySpent(fuelUpAverages.getTotalMoneySpent() - (fuelUp.getPrice() * fuelUp.getAmount()));
		fuelUpAverages.setTotalFuelUps(fuelUpAverages.getTotalFuelUps() - 1);

		long rowID = storeFuelUpAverage(fuelUpAverages, true);
		
		return rowID;
	}
	
	private long storeFuelUpAverage(FuelUpAverages fuelUpAverages, boolean replaceAverages)
	{
		ContentValues values = new ContentValues();
		values.put(DbHelper.FUEL_UP_AVERAGES_VEHICLE_ID, fuelUpAverages.getVehicleId());
		values.put(DbHelper.FUEL_UP_AVERAGES_LAST_FUEL_UP_CONSUMPTION, fuelUpAverages.getLastFuelUpConsumption());
		values.put(DbHelper.FUEL_UP_AVERAGES_LAST_FUEL_UP_PRICE, fuelUpAverages.getLastFuelUpPrice());
		values.put(DbHelper.FUEL_UP_AVERAGES_TOTAL_DISTANCE, fuelUpAverages.getTotalDistance());
		values.put(DbHelper.FUEL_UP_AVERAGES_TOTAL_FUEL_CONSUMED, fuelUpAverages.getTotalFuelConsumed());
		values.put(DbHelper.FUEL_UP_AVERAGES_TOTAL_MONEY_SPENT, fuelUpAverages.getTotalMoneySpent());
		values.put(DbHelper.FUEL_UP_AVERAGES_TOTAL_FUEL_UPS, fuelUpAverages.getTotalFuelUps());
		
		long rowID = -1;
		if (replaceAverages) {
			//Fuel up averages already exist for this vehicle
			values.put(DbHelper.FUEL_UP_AVERAGES_ID, fuelUpAverages.getId());
			rowID = db.replace(DbHelper.FUEL_UP_AVERAGES_TABLE_NAME, null, values);
		} else {
			//This vehicle has no averages yet...
			rowID = db.insert(DbHelper.FUEL_UP_AVERAGES_TABLE_NAME, null, values);
		}
		
		return rowID;
	}
	
//	private FuelUpAverages getFuelUpById(long id) {
//		FuelUpAverages fuelUpAverages = null;
//		if (id <= 0) {
//			return fuelUpAverages;
//		}
//		
//		Cursor cursor = db.query(
//				DbHelper.FUEL_UP_AVERAGES_TABLE_NAME, 
//				null, //(Returning) Columns
//    			DbHelper.FUEL_UP_AVERAGES_ID + " = " + id, //Selection (WHERE)
//    			null, //SelectionArgs (You may include ?s in selection, which will be replaced by the values from selectionArgs)
//    			null, //GroupBy
//    			null, //Having
//    			null, //OrderBy
//				"1" //Limit
//				);
//		cursor.moveToFirst();
//		fuelUpAverages = cursorToFuelUpAverage(cursor);
//	    cursor.close();
//		
//		return fuelUpAverages;
//	}
	
	public FuelUpAverages getFuelUpAverageByVehicleId(long vehicleId) {
		FuelUpAverages fuelUpAverages = null;
		if (vehicleId <= 0) {
			return fuelUpAverages;
		}
		
	    Cursor cursor = db.query(
				DbHelper.FUEL_UP_AVERAGES_TABLE_NAME, 
				null, //(Returning) Columns
    			DbHelper.FUEL_UP_AVERAGES_VEHICLE_ID + " = " + vehicleId, //Selection (WHERE)
    			null, //SelectionArgs (You may include ?s in selection, which will be replaced by the values from selectionArgs)
    			null, //GroupBy
    			null, //Having
    			null, //OrderBy
				null //Limit
				);
	    
	    cursor.moveToFirst();
		fuelUpAverages = cursorToFuelUpAverage(cursor);
	    cursor.close();
	    
		return fuelUpAverages;
	}
	
	private FuelUpAverages cursorToFuelUpAverage(Cursor cursor) {
		FuelUpAverages fuelUpAverages = null;
		if (cursor == null || cursor.isBeforeFirst() || cursor.isAfterLast() || cursor.isClosed()) {
			return fuelUpAverages;
		}

		fuelUpAverages = new FuelUpAverages(cursor.getLong(cursor.getColumnIndex(DbHelper.FUEL_UP_AVERAGES_VEHICLE_ID)));
		fuelUpAverages.setId(cursor.getLong(cursor.getColumnIndex(DbHelper.FUEL_UP_AVERAGES_ID)));
		fuelUpAverages.setLastFuelUpConsumption(cursor.getDouble(cursor.getColumnIndex(DbHelper.FUEL_UP_AVERAGES_LAST_FUEL_UP_CONSUMPTION)));
		fuelUpAverages.setLastFuelUpPrice(cursor.getDouble(cursor.getColumnIndex(DbHelper.FUEL_UP_AVERAGES_LAST_FUEL_UP_PRICE)));
		fuelUpAverages.setTotalDistance(cursor.getDouble(cursor.getColumnIndex(DbHelper.FUEL_UP_AVERAGES_TOTAL_DISTANCE)));
		fuelUpAverages.setTotalFuelConsumed(cursor.getDouble(cursor.getColumnIndex(DbHelper.FUEL_UP_AVERAGES_TOTAL_FUEL_CONSUMED)));
		fuelUpAverages.setTotalMoneySpent(cursor.getDouble(cursor.getColumnIndex(DbHelper.FUEL_UP_AVERAGES_TOTAL_MONEY_SPENT)));
		fuelUpAverages.setTotalFuelUps(cursor.getLong(cursor.getColumnIndex(DbHelper.FUEL_UP_AVERAGES_TOTAL_FUEL_UPS)));
		
		return fuelUpAverages;
	}

}