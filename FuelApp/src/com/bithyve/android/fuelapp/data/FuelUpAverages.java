package com.bithyve.android.fuelapp.data;


public class FuelUpAverages {

	private long id;
	private long vehicleId;
	private double lastFuelUpConsumption;
	private double lastFuelUpPrice;
	private double totalDistance;
	private double totalFuelConsumed;
	private double totalMoneySpent;
	private long totalFuelUps;
	
	public FuelUpAverages(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public double getLastFuelUpConsumption() {
		return lastFuelUpConsumption;
	}

	public void setLastFuelUpConsumption(double lastFuelUpConsumption) {
		this.lastFuelUpConsumption = lastFuelUpConsumption;
	}

	public double getLastFuelUpPrice() {
		return lastFuelUpPrice;
	}

	public void setLastFuelUpPrice(double lastFuelUpPrice) {
		this.lastFuelUpPrice = lastFuelUpPrice;
	}

	public double getAverageFuelUpConsumption() {
		double averageFuelUpConsumption;
		averageFuelUpConsumption = getTotalDistance() / getTotalFuelConsumed();
		return averageFuelUpConsumption;
	}

	public double getAverageFuelUpPrice() {
		double averageFuelUpPrice;
		averageFuelUpPrice = getTotalMoneySpent() / getTotalFuelUps();
		return averageFuelUpPrice;
	}

	public double getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}

	public double getTotalFuelConsumed() {
		return totalFuelConsumed;
	}

	public void setTotalFuelConsumed(double totalFuelConsumed) {
		this.totalFuelConsumed = totalFuelConsumed;
	}

	public double getTotalMoneySpent() {
		return totalMoneySpent;
	}

	public void setTotalMoneySpent(double totalMoneySpent) {
		this.totalMoneySpent = totalMoneySpent;
	}

	public long getTotalFuelUps() {
		return totalFuelUps;
	}

	public void setTotalFuelUps(long totalFuelUps) {
		this.totalFuelUps = totalFuelUps;
	}
	
}
