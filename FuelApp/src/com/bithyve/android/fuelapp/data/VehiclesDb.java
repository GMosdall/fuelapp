package com.bithyve.android.fuelapp.data;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.bithyve.android.fuelapp.preferences.DistanceUnit;
import com.bithyve.android.fuelapp.preferences.EfficiencyUnit;
import com.bithyve.android.fuelapp.preferences.TrackMeterUnit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

//TODO: implement delete vehicle
public class VehiclesDb {
	
	private DbHelper dbHelper;
	private SQLiteDatabase db;
	private Context ctx;
	
	public VehiclesDb(Context ctx) {
		dbHelper = new DbHelper(ctx);
		this.ctx = ctx;
	}
	
	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public long storeVehicle(Vehicle vehicle, boolean newImage) {
		
		ContentValues values = new ContentValues();
		values.put(DbHelper.VEHICLE_NAME, vehicle.getName());
		values.put(DbHelper.VEHICLE_MAKE, vehicle.getMake());
		values.put(DbHelper.VEHICLE_MODEL, vehicle.getModel());
		values.put(DbHelper.VEHICLE_YEAR, vehicle.getYear());
		values.put(DbHelper.VEHICLE_FUEL_TYPE, vehicle.getFuelType());
		values.put(DbHelper.VEHICLE_NOTES, vehicle.getNotes());
		
		//Add display units
		try {
			values.put(DbHelper.VEHICLE_TRACK_METER_UNIT, vehicle.getStorageTrackMeterUnit().getId());
		} catch (NullPointerException e) {
			Log.i("FuelApp", "Nothing wrong here, just using the Global Preferences for Vehicle TrackMeter Unit");
		}
		try {
			values.put(DbHelper.VEHICLE_DISTANCE_UNIT, vehicle.getStorageDistanceUnit().getId());
		} catch (NullPointerException e) {
			Log.i("FuelApp", "Nothing wrong here, just using the Global Preferences for Vehicle Distance Unit");
		}
		
		try {
			values.put(DbHelper.VEHICLE_EFFICIENCY_UNIT, vehicle.getStorageEfficiencyUnit().getId());
		} catch (NullPointerException e) {
			Log.i("FuelApp", "Nothing wrong here, just using the Global Preferences for Vehicle Efficiency Unit");
		}
		
		if (vehicle.getImage() != null) {
			if (newImage) {
				//Convert Drawable to byte[] so we can store it as a blob
				BitmapDrawable bitDw = (BitmapDrawable) vehicle.getImage();
				Bitmap bitmap = bitDw.getBitmap();
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				byte[] imageBlob = stream.toByteArray();
				values.put(DbHelper.VEHICLE_IMAGE, imageBlob);
				
			} else {
				if (vehicle.getId() > 0) {
					byte[] imageBlob = getImageBlobById(vehicle.getId());
					values.put(DbHelper.VEHICLE_IMAGE, imageBlob);
				}
			}
		}
		
		long rowID = -1;
		if (vehicle.getId() > 0) {
			//Vehicle already exists! Replace with current data
			values.put(DbHelper.VEHICLE_ID, vehicle.getId());
			rowID = db.replace(DbHelper.VEHICLES_TABLE_NAME, null, values);
		} else {
			//A new vehicle, insert as new data
			rowID = db.insert(DbHelper.VEHICLES_TABLE_NAME, null, values);
		}
		
		return rowID;
	}
	
	public Vehicle getVehicleById(long id) {
		Vehicle vehicle = null;
		if (id <= 0) {
			return vehicle;
		}
		
		Cursor cursor = db.query(
				DbHelper.VEHICLES_TABLE_NAME, 
				null, //(Returning) Columns
    			DbHelper.VEHICLE_ID + " = " + id, //Selection (WHERE)
    			null, //SelectionArgs (You may include ?s in selection, which will be replaced by the values from selectionArgs)
    			null, //GroupBy
    			null, //Having
    			null, //OrderBy
				"1"
				);
		cursor.moveToFirst();
		vehicle = cursorToVehicle(cursor);
	    cursor.close();
		
		return vehicle;
	}
	
	public List<Vehicle> getAllVehicles() {
		List<Vehicle> list = new ArrayList<Vehicle>();
		
	    Cursor cursor = db.query(DbHelper.VEHICLES_TABLE_NAME,
	    		DbHelper.VEHICLE_ALL_COLUMNS, null, null, null, null, null);
	    
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	Vehicle vehicle = cursorToVehicle(cursor);
	    	list.add(vehicle);
	    	cursor.moveToNext();
	    }
	    cursor.close();
	    
		return list;
	}
	
	private Bitmap convertByteArrayToBitmap(byte[] imgBytes) {
		Bitmap bitmap = null;
		
		if (imgBytes != null && imgBytes.length > 0) {
			bitmap = BitmapFactory.decodeByteArray(
					imgBytes, 0, imgBytes.length);
		}
		
		return bitmap;
	}
	
	private Vehicle cursorToVehicle(Cursor cursor) {
		Vehicle vehicle = null;
		if (cursor == null || cursor.isBeforeFirst() || cursor.isAfterLast() || cursor.isClosed()) {
			return vehicle;
		}
		
		vehicle = new Vehicle(cursor.getString(cursor.getColumnIndex(DbHelper.VEHICLE_NAME)));
		vehicle.setId(cursor.getLong(cursor.getColumnIndex(DbHelper.VEHICLE_ID)));
		vehicle.setMake(cursor.getString(cursor.getColumnIndex(DbHelper.VEHICLE_MAKE)));
		vehicle.setModel(cursor.getString(cursor.getColumnIndex(DbHelper.VEHICLE_MODEL)));
		vehicle.setYear(cursor.getInt(cursor.getColumnIndex(DbHelper.VEHICLE_YEAR)));
		vehicle.setFuelType(cursor.getString(cursor.getColumnIndex(DbHelper.VEHICLE_FUEL_TYPE)));
		vehicle.setNotes(cursor.getString(cursor.getColumnIndex(DbHelper.VEHICLE_NOTES)));
		
		//Display units
		int trackMeterUnitColumnIndex = cursor.getColumnIndex(DbHelper.VEHICLE_TRACK_METER_UNIT);
		if (!cursor.isNull(trackMeterUnitColumnIndex)) {
			TrackMeterUnit trackMeterUnit = TrackMeterUnit.newInstance(cursor.getInt(trackMeterUnitColumnIndex));
			vehicle.setTrackMeterUnit(trackMeterUnit);
		}
		
		int distanceUnitColumnIndex = cursor.getColumnIndex(DbHelper.VEHICLE_DISTANCE_UNIT);
		if (!cursor.isNull(distanceUnitColumnIndex)) {
			DistanceUnit distanceUnit = DistanceUnit.newInstance(cursor.getInt(distanceUnitColumnIndex));
			vehicle.setDistanceUnit(distanceUnit);
		}

		int efficiencyUnitColumnIndex = cursor.getColumnIndex(DbHelper.VEHICLE_EFFICIENCY_UNIT);
		if (!cursor.isNull(efficiencyUnitColumnIndex)) {
			EfficiencyUnit efficiencyUnit = EfficiencyUnit.newInstance(cursor.getInt(efficiencyUnitColumnIndex));
			vehicle.setEfficiencyUnit(efficiencyUnit);
		}
		
		byte[] imgBytes = cursor.getBlob(cursor.getColumnIndex(DbHelper.VEHICLE_IMAGE));
		if (imgBytes != null && imgBytes.length > 0) {
			Bitmap bitmap = convertByteArrayToBitmap(imgBytes);
			Drawable image = new BitmapDrawable(ctx.getResources(), bitmap);
			vehicle.setImage(image);
		}
		
		return vehicle;
	}
	
	private byte[] getImageBlobById(long id) {
		byte[] blob = null;
		if (id <= 0) {
			return blob;
		}
		
		Cursor cursor = db.query(
				DbHelper.VEHICLES_TABLE_NAME, 
				null, //(Returning) Columns
    			DbHelper.VEHICLE_ID + " = " + id, //Selection (WHERE)
    			null, //SelectionArgs (You may include ?s in selection, which will be replaced by the values from selectionArgs)
    			null, //GroupBy
    			null, //Having
    			null, //OrderBy
				"1"
				);
		cursor.moveToFirst();
		
		if (cursor == null || cursor.isBeforeFirst() || cursor.isAfterLast() || cursor.isClosed()) {
			return blob;
		}
		blob = cursor.getBlob(cursor.getColumnIndex(DbHelper.VEHICLE_IMAGE));
		
		return blob;
	}
}
