package com.bithyve.android.fuelapp.scrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import com.nirhart.parallaxscroll.views.ParallaxScrollView;

public class NotifyingParallaxScrollView extends ParallaxScrollView {

    private OnScrollChangedListener mOnScrollChangedListener;

	public NotifyingParallaxScrollView(Context context) {
		super(context);
	}

	public NotifyingParallaxScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public NotifyingParallaxScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
    public interface OnScrollChangedListener {
        void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt);
    }

    public void setOnScrollChangedListener(OnScrollChangedListener listener) {
        mOnScrollChangedListener = listener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mOnScrollChangedListener != null) {
            mOnScrollChangedListener.onScrollChanged(this, l, t, oldl, oldt);
        }
    }

}
