package com.bithyve.android.fuelapp.vehicles;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.FuelUpAverages;
import com.bithyve.android.fuelapp.utils.FuelUpAveragesUtil;

public class AveragesPagerAdapter extends FragmentPagerAdapter {
	
	private FuelUpAverages fuelUpAverages;
	private Context context;

	@SuppressLint("DefaultLocale")
	private static String getSingleTitle(Context context) {
		String title;
		title = context.getString(R.string.Last_fuel_up).toUpperCase();
		return title;
	}

	@SuppressLint("DefaultLocale")
	private static String getSingleLeftSuperText(FuelUpAverages fuelUpAverages) {
		String text;
		text = String.format("%.2f", fuelUpAverages.getLastFuelUpConsumption());
		return text;
	}

	@SuppressLint("DefaultLocale")
	private static String getSingleLeftSubText(Context context) {
		String text;
		text = context.getString(R.string.Kilometres_per_litre_abb).toUpperCase();
		return text;
	}
	
	@SuppressLint("DefaultLocale")
	private static String getSingleRightSuperText(FuelUpAverages fuelUpAverages) {
		String text;
		text = String.format("%.2f", fuelUpAverages.getLastFuelUpPrice());
		return text;
	}
	
	@SuppressLint("DefaultLocale")
	private static String getSingleRightSubText(Context context) {
		String text;
		text = context.getString(R.string.euro).toUpperCase();
		return text;
	}
	
	private static int getSingleLeftSuperTextColor(FuelUpAverages fuelUpAverages) {
		int color;
		boolean isLastBetterThanAverage = FuelUpAveragesUtil.isLastFuelUpBetterThanAverage(fuelUpAverages);
		if (isLastBetterThanAverage) {
			color = 0xFF33CC66;
		} else {
			color = 0xFF000000;
		}
		
		return color;
	}
	
	public static View getSingleAveragesView(FuelUpAverages fuelUpAverages, Context context) {
		View averagesView;
		
		String title = getSingleTitle(context);
		String leftSuperText = getSingleLeftSuperText(fuelUpAverages);
		String leftSubText = getSingleLeftSubText(context);
		String rightSuperText = getSingleRightSuperText(fuelUpAverages);
		String rightSubText = getSingleRightSubText(context);
		
		int leftSuperTextColor = getSingleLeftSuperTextColor(fuelUpAverages);
		
		LayoutInflater inflater = LayoutInflater.from(context);
		
		averagesView = AveragesPagerFragment.getSingleAveragesView(inflater, title, leftSuperText, leftSuperTextColor, leftSubText, rightSuperText, rightSubText);
		
		return averagesView;
	}

	public AveragesPagerAdapter(FragmentManager fm, FuelUpAverages fuelUpAverages, Context context) {
		super(fm);
		this.fuelUpAverages = fuelUpAverages;
		this.context = context;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public Fragment getItem(int position) {
        
        //TODO: Show total spent?
        //TODO: Show total distance tracked?
		
		int leftSuperTextColor = 0xFF000000;
		String title, leftSuperText, leftSubText, rightSuperText, rightSubText;
		switch (position) {
			case 0:
				title = getSingleTitle(context);
				leftSuperText = getSingleLeftSuperText(fuelUpAverages);
				leftSubText = getSingleLeftSubText(context);
				rightSuperText = getSingleRightSuperText(fuelUpAverages);
				rightSubText = getSingleRightSubText(context);
				
				leftSuperTextColor = getSingleLeftSuperTextColor(fuelUpAverages);
				break;
	
			case 1:
				title = context.getString(R.string.Average_over_x_fuel_ups, fuelUpAverages.getTotalFuelUps()).toUpperCase();
				leftSuperText = String.format("%.2f", fuelUpAverages.getAverageFuelUpConsumption());
				leftSubText = context.getString(R.string.Kilometres_per_litre_abb).toUpperCase();
				rightSuperText = String.format("%.2f", fuelUpAverages.getAverageFuelUpPrice());
				rightSubText = context.getString(R.string.euro).toUpperCase();
				break;
				
			case 2:
				title = context.getString(R.string.Average_over_x_fuel_ups, fuelUpAverages.getTotalFuelUps()).toUpperCase();
				double distancePerFuelUp = fuelUpAverages.getTotalDistance() / fuelUpAverages.getTotalFuelUps();
				long distanceShown = Math.round(distancePerFuelUp);
				leftSuperText = String.valueOf(distanceShown);
				leftSubText = context.getString(R.string.km_fuel_up).toUpperCase();
				double pricePerKm = fuelUpAverages.getTotalMoneySpent() / fuelUpAverages.getTotalDistance();
				rightSuperText = String.format("%.2f", pricePerKm);
				rightSubText = context.getString(R.string.euro_km).toUpperCase();
				break;
			
			default:
				title = "-";
				leftSuperText = "-";
				leftSubText = "-";
				rightSuperText = "-";
				rightSubText = "-";
				break;
		}
		Fragment fragment = AveragesPagerFragment.newInstance(title, leftSuperText, leftSuperTextColor, leftSubText, rightSuperText, rightSubText);
		
		return fragment;
	}

	@Override
	public int getCount() {
		return 3;
	}

}
