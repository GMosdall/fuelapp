package com.bithyve.android.fuelapp.vehicles;

import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.FuelUp;
import com.bithyve.android.fuelapp.data.FuelUpAverages;
import com.bithyve.android.fuelapp.data.FuelUpAveragesDb;
import com.bithyve.android.fuelapp.data.FuelUpsDb;
import com.bithyve.android.fuelapp.data.Vehicle;
import com.bithyve.android.fuelapp.data.VehiclesDb;
import com.bithyve.android.fuelapp.scrollview.NotifyingParallaxScrollView;
import com.bithyve.android.fuelapp.utils.DateUtil;
import com.bithyve.android.fuelapp.vehicles.fuelup.EditFuelUpActivity;
import com.bithyve.android.fuelapp.vehicles.fuelup.FuelUpsOverview;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;
import com.echo.holographlibrary.Line;
import com.echo.holographlibrary.LineGraph;
import com.echo.holographlibrary.LinePoint;
import com.viewpagerindicator.CirclePageIndicator;

public class VehicleStatisticsFragment extends Fragment {

	private final int ADD_FUEL_UP = 1000;
	private final int FUEL_UPS_OVERVIEW = 1001;
	
	public static final String VEHICLE_ID = "vehicleId";
	private Vehicle vehicle;
	private ImageView mHeaderView;
	private View mView;
	private ActionBar mActionBar;
	private Drawable mActionBarBackgroundDrawable;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		
		mView = inflater.inflate(R.layout.vehicle_statistics_fragment, null);
		
		Bundle extras = getArguments();
		if (extras == null) {
			return mView;
		}
		long vehicleId = extras.getLong(VEHICLE_ID, -1);
		
		//Set vehicle title
		VehiclesDb db = new VehiclesDb(getActivity());
		db.open();
		vehicle = db.getVehicleById(vehicleId);
		db.close();
		
		TextView vehicleTitleView = (TextView) mView.findViewById(R.id.vehicleTitle);
		vehicleTitleView.setText(vehicle.getName());
		
		mActionBar = getActivity().getActionBar();
		mActionBar.setTitle(R.string.Statistics);
		
		//Set the actionbar background
		mActionBarBackgroundDrawable = getResources().getDrawable(R.drawable.ab_background_blue);
        mActionBar.setBackgroundDrawable(mActionBarBackgroundDrawable);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            mActionBarBackgroundDrawable.setCallback(mDrawableCallback);
        }
        mActionBarBackgroundDrawable.setAlpha(0);

        //Set the header photo
		mHeaderView = (ImageView) mView.findViewById(R.id.imageHeader);
		Drawable vehicleImage = vehicle.getImage();
		if (vehicleImage != null) {
			mHeaderView.setImageDrawable(vehicleImage);
		}
		
		NotifyingParallaxScrollView scrollView = (NotifyingParallaxScrollView) mView.findViewById(R.id.vehicleStatisticsContainer);
        scrollView.setOnScrollChangedListener(mOnScrollChangedListener);
		
		//The options are set in onCreateOptionsMenu
		setHasOptionsMenu(true);
		
		updateView();
		
		
		return mView;
	}

	/*
	 * Begin of FadingActionBar shizzle
	 */
    private Drawable.Callback mDrawableCallback = new Drawable.Callback() {
        @Override
        public void invalidateDrawable(Drawable who) {
            mActionBar.setBackgroundDrawable(who);
        }

        @Override
        public void scheduleDrawable(Drawable who, Runnable what, long when) {
        }

        @Override
        public void unscheduleDrawable(Drawable who, Runnable what) {
        }
    };

    private NotifyingParallaxScrollView.OnScrollChangedListener mOnScrollChangedListener = new NotifyingParallaxScrollView.OnScrollChangedListener() {
        public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
            onNewScroll(t);
        }
    };

    private void onNewScroll(int scrollPosition) {
        if (mActionBar == null) {
            return;
        }

        int currentHeaderHeight = mHeaderView.getHeight();
        int headerHeight = currentHeaderHeight - mActionBar.getHeight();
        float ratio = (float) Math.min(Math.max(scrollPosition, 0), headerHeight) / headerHeight;
        int newAlpha = (int) (ratio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
    }
    /*
     * End of FadingActionBar shizzle
     */
	
	/**
	 * Is being used by MainFragmentActivity to determine whether this fragment should be
	 * reloaded with new vehicle information or not.
	 * @return long - the vehicle ID
	 */
	public long getVehicleId() {
		long vehicleId = -1;
		if (vehicle != null) {
			vehicleId = vehicle.getId();
		}
		return vehicleId;
	}
	
	@SuppressLint("DefaultLocale")
	private void updateView() {

		//Check whether there are fuel-ups
		FuelUpsDb db = new FuelUpsDb(getActivity().getApplicationContext());
		db.open();
		List<FuelUp> fuelUps = db.getAllFuelUpsByVehicle(vehicle.getId());
		db.close();
		
		ImageView infoButton = (ImageView) mView.findViewById(R.id.vehicleInfoButton);
		Button showAllButton = (Button) mView.findViewById(R.id.showAllFuelUpsButton);
		View lineGraphContainer = mView.findViewById(R.id.lineGraphContainer);
		View barGraphContainer = mView.findViewById(R.id.barGraphContainer);
		
		if (fuelUps.size() > 0) {
			
			//Enable the vehicle info imageView as a button
			if (!infoButton.hasOnClickListeners()) {
				registerInfoButton(infoButton);
				infoButton.setVisibility(View.VISIBLE);
			}
			
			//Update the averages
			ViewPager pager = (ViewPager)mView.findViewById(R.id.averagesPager);
			CirclePageIndicator indicator = (CirclePageIndicator)mView.findViewById(R.id.averagesPageIndicator);

			FuelUpAveragesDb averagesDb = new FuelUpAveragesDb(getActivity().getApplicationContext());
			averagesDb.open();
			FuelUpAverages fuelUpAverages = averagesDb.getFuelUpAverageByVehicleId(vehicle.getId());
			averagesDb.close();
			
			//Update the averages
			showFuelUpsAverages(pager, indicator, fuelUpAverages);
			View pagerContainer = mView.findViewById(R.id.averagesPagerContainer);
			pagerContainer.setVisibility(View.VISIBLE);
			
			
			//Update the line graph
			LineGraph lineGraph = (LineGraph) mView.findViewById(R.id.fuelUpsLineGraph);
			if (lineGraph.getLines().size() > 0) {
				lineGraph.removeAllLines();
			}
			showFuelUpsLineGraph(lineGraph, fuelUps);
			lineGraphContainer.setVisibility(View.VISIBLE);
			
			//Update the line graph caption
			TextView graphCaption = (TextView) mView.findViewById(R.id.lineGraphCaption);
			String lineCaptionText = getString(R.string.Recent_fuel_ups_caption).toUpperCase();
			graphCaption.setText(lineCaptionText);
			
			
			//Update the bar graph
			BarGraph barGraph = (BarGraph) mView.findViewById(R.id.fuelUpsBarGraph);
			if (barGraph.getBars().size() > 0) {
				barGraph.getBars().clear();
			}
			showFuelUpsBarGraph(barGraph, fuelUps);
			barGraphContainer.setVisibility(View.VISIBLE);
			//TODO: After a fuel-up the bar graph is now only refreshed after a "touch", this should be automatically
			
			//Update the bar graph caption
			TextView barCaption = (TextView) mView.findViewById(R.id.barGraphCaption);
			String barCaptionText = getString(R.string.Distance_per_month_caption).toUpperCase();
			barCaption.setText(barCaptionText);
			
			
			//Show all fuel-ups button
			if (!showAllButton.hasOnClickListeners()) {
				registerShowAllButton(showAllButton);
				showAllButton.setVisibility(View.VISIBLE);
			}
		} else {
			infoButton.setVisibility(View.GONE);
			showAllButton.setVisibility(View.GONE);
			lineGraphContainer.setVisibility(View.GONE);
			barGraphContainer.setVisibility(View.GONE);
			
			//TODO: show message "Add your first Fuel-up!"
		}
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	private void showFuelUpsAverages(ViewPager pager, CirclePageIndicator indicator, FuelUpAverages fuelUpAverages) {
		
		//ChildFragmentManager is only available from API level 17
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
			AveragesPagerAdapter adapter = new AveragesPagerAdapter(getChildFragmentManager(), fuelUpAverages, getActivity().getApplicationContext());
			
			pager.setAdapter(adapter);
			
	        indicator.setViewPager(pager);
	        //Using the same GRAY as used by the dots in the LineGraph
	        indicator.setPageColor(Color.WHITE);
	        indicator.setFillColor(Color.GRAY);
	        indicator.setStrokeColor(Color.GRAY);
		} else {
			pager.setVisibility(View.GONE);
			View singleAveragesView = AveragesPagerAdapter.getSingleAveragesView(fuelUpAverages, getActivity());
			ViewGroup legacyContainer = (ViewGroup) mView.findViewById(R.id.legacyAveragesPagerContainer);
			legacyContainer.addView(singleAveragesView);
			legacyContainer.setVisibility(View.VISIBLE);
		}
	}
	
	private void showFuelUpsLineGraph(LineGraph lineGraph, List<FuelUp> fuelUps) {
		Line line = new Line();
		//Original color: FFBB33
		//Kuler H-E-A-T yellow: F0A024
		line.setColor(Color.parseColor("#FFBB33"));
		
		double lowest = 0;
		double highest = 0;
		boolean firstPoint = true;
		int i = fuelUps.size() > 10 ? fuelUps.size() - 10 : 0; //Only show the last 10 fuelUps
		for (; i < fuelUps.size(); i++) {
			FuelUp fuelUp = fuelUps.get(i);
			double consumption;
			consumption = fuelUp.getDistance() / fuelUp.getAmount();
			
			LinePoint point = new LinePoint();
			point.setX(i);
			point.setY((float)consumption);
			String label = DateUtil.getDayMonthString(fuelUp.getDate());
			point.setLabel(label);
			line.addPoint(point);
			
			if (consumption < lowest || firstPoint) {
				lowest = consumption;
				firstPoint = false;
			}
			if (consumption > highest) {
				highest = consumption;
			}
		}
		
		//Create a margin for the graph
		lowest--;
		highest++;

		lineGraph.setGridColor(Color.BLACK);
		lineGraph.setTextColor(Color.BLACK);
		lineGraph.setTextSize(30);
		lineGraph.showHorizontalGrid(true);
		lineGraph.showMinAndMaxValues(true);
		lineGraph.setRangeY((float)lowest, (float)highest);
		lineGraph.addLine(line);
	}
	
	private void showFuelUpsBarGraph(BarGraph barGraph, List<FuelUp> fuelUps) {
		
		//Make a copy of the list, so the original will not be manipulated
		//TODO: sort list on date
		List<FuelUp> reverseList = new ArrayList<FuelUp>(fuelUps);
		Collections.reverse(reverseList);
		
		ArrayList<Bar> bars = new ArrayList<Bar>();
		
		int lastMonth = 0;
		double monthDistance = 0;
		int numberOfMonths = 0;
		boolean postProcessLastMonth = true;
		for (FuelUp fuelUp : reverseList) {
			
			Date fuelDate = fuelUp.getDate();
			Calendar cal = new GregorianCalendar();
			cal.setTime(fuelDate);
			int month = cal.get(Calendar.MONTH);
			
			if (month == lastMonth && numberOfMonths > 0) {
				monthDistance += fuelUp.getDistance();
			} else {
				if (numberOfMonths > 0) {
					//Set the info of the previous month
					Bar bar = getMonthDistanceBar(lastMonth, monthDistance);
					bars.add(0, bar);
					
					//Was this the last month?
					if (numberOfMonths >= 5) {
						postProcessLastMonth = false;
						break;
					}
				}
				
				//Set the info of this month
				lastMonth = month;
				monthDistance = fuelUp.getDistance();
				numberOfMonths++;
			}
		}
		
		//Get the bar of the last month that wasn't processed in the loop
		if (postProcessLastMonth) {
			Bar bar = getMonthDistanceBar(lastMonth, monthDistance);
			bars.add(0, bar);
		}

		DecimalFormat df = new DecimalFormat("0");
		barGraph.setDecimalFormat(df);
		barGraph.setUnit("");
		barGraph.appendUnit(true);
		barGraph.setBars(bars);
		
		/*
		 * Ideas for the bar chart:
		 * 1- The average consumption per month
		 * 2- The average distance per month <-- That's wat it displays now
		 * 3- Maybe the above two as sticky bars next to each other
		 */
	}
	
	private Bar getMonthDistanceBar(int month, double distance) {
		Bar bar = new Bar();
		//Original color: FFBB33
		//Kuler H-E-A-T yellow: F0A024
		bar.setColor(Color.parseColor("#FFBB33")); //TODO: tweak the color
		String monthName = new DateFormatSymbols().getShortMonths()[month];
		bar.setName(monthName);
		bar.setValue((float) distance);
		
		return bar;
	}
	
	private void registerInfoButton(ImageView infoButton) {
		infoButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showVehicleInfoOnClick(v);
			}
		});
	}
	
	private void showVehicleInfoOnClick(View view) {
		Intent intent = new Intent(getActivity(), VehicleTotalsInfoActivity.class);
		intent.putExtra(VehicleTotalsInfoActivity.VEHICLE_ID, this.vehicle.getId());
		startActivity(intent);
	}
	
	private void registerShowAllButton(Button showAllButton) {
		//Register the button listener
		showAllButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showAllFuelUpsOnClick(v);
			}
		});
	}
	
	private void showAllFuelUpsOnClick(View view) {
		Intent intent = new Intent(getActivity(), FuelUpsOverview.class);
		intent.putExtra(FuelUpsOverview.VEHICLE_ID, this.vehicle.getId());
		startActivityForResult(intent, FUEL_UPS_OVERVIEW);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fuel_up_item, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean itemSelected;
		
		switch (item.getItemId()) {
		case R.id.fuelUpItem:
    		Intent intent = new Intent(getActivity(), EditFuelUpActivity.class);
    		intent.putExtra(EditFuelUpActivity.VEHICLE_ID, vehicle.getId());
    		startActivityForResult(intent, ADD_FUEL_UP);
    		
			itemSelected = true;
			break;

		default:
			itemSelected = super.onOptionsItemSelected(item);
			break;
		}
		
		return itemSelected;
	}

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	
        if (resultCode != Activity.RESULT_OK) {
        	return;
        }
        
        if (requestCode == ADD_FUEL_UP) {
        	updateView();
        } else if (requestCode == FUEL_UPS_OVERVIEW) {
        	updateView();
		}
    }
}
