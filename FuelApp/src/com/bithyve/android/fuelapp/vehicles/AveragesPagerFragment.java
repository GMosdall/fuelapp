package com.bithyve.android.fuelapp.vehicles;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.R.layout;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AveragesPagerFragment extends Fragment {

	private static String TITLE = "title";
	private static String LEFT_SUPER_TEXT = "leftSuperText";
	private static String LEFT_SUPER_TEXT_COLOR = "leftSuperTextColor";
	private static String LEFT_SUB_TEXT = "leftSubText";
	private static String RIGHT_SUPER_TEXT = "rightSuperText";
	private static String RIGHT_SUB_TEXT = "rightSubText";
	
	static AveragesPagerFragment newInstance(String title, String leftSuperText, String leftSubText, String rightSuperText, String rightSubText) {
		int leftSuperTextColor = 0xFF000000; //Defaults to black
		AveragesPagerFragment fragment = AveragesPagerFragment.newInstance(title, leftSuperText, leftSuperTextColor, leftSubText, rightSuperText, rightSubText);
		return fragment;
	}
	
	static AveragesPagerFragment newInstance(String title, String leftSuperText, int leftSuperTextColor, String leftSubText, String rightSuperText, String rightSubText) {
		AveragesPagerFragment fragment = new AveragesPagerFragment();
		
		Bundle args = new Bundle();
		args.putString(TITLE, title);
		args.putString(LEFT_SUPER_TEXT, leftSuperText);
		args.putInt(LEFT_SUPER_TEXT_COLOR, leftSuperTextColor);
		args.putString(LEFT_SUB_TEXT, leftSubText);
		args.putString(RIGHT_SUPER_TEXT, rightSuperText);
		args.putString(RIGHT_SUB_TEXT, rightSubText);
		
		fragment.setArguments(args);
		
		return fragment;
	}
	
	public static View getSingleAveragesView(LayoutInflater inflater, String title, String leftSuperText, int leftSuperTextColor, String leftSubText, String rightSuperText, String rightSubText) {
		View view = inflater.inflate(layout.averages_pager_fragment, null, false);
		
		TextView titleTextView = (TextView)view.findViewById(R.id.averagesTitle);
		titleTextView.setText(title);
		
		TextView leftSuperTextView = (TextView)view.findViewById(R.id.averagesLeftSuperText);
		leftSuperTextView.setText(leftSuperText);
		leftSuperTextView.setTextColor(leftSuperTextColor);
		
		TextView leftSubTextView = (TextView)view.findViewById(R.id.averagesLeftSubText);
		leftSubTextView.setText(leftSubText);
		
		TextView rightSuperTextView = (TextView)view.findViewById(R.id.averagesRightSuperText);
		rightSuperTextView.setText(rightSuperText);
		
		TextView rightSubTextView = (TextView)view.findViewById(R.id.averagesRightSubText);
		rightSubTextView.setText(rightSubText);
		
		return view;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		String title = getArguments().getString(TITLE);
		String leftSuperText = getArguments().getString(LEFT_SUPER_TEXT);
		int leftSuperTextColor = getArguments().getInt(LEFT_SUPER_TEXT_COLOR);
		String leftSubText = getArguments().getString(LEFT_SUB_TEXT);
		String rightSuperText = getArguments().getString(RIGHT_SUPER_TEXT);
		String rightSubText = getArguments().getString(RIGHT_SUB_TEXT);
		
		View view = getSingleAveragesView(inflater, title, leftSuperText, leftSuperTextColor, leftSubText, rightSuperText, rightSubText);
		
		return view;
	}

}
