package com.bithyve.android.fuelapp.vehicles;

import java.text.NumberFormat;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.FuelUpAverages;
import com.bithyve.android.fuelapp.data.FuelUpAveragesDb;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class VehicleTotalsInfoActivity extends Activity {
	
	public static final String VEHICLE_ID = "vehicleId";

	private ActionBar mActionBar;
	private long mVehicleId;
	private NumberFormat mNumberFormat;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vehicle_totals_info_activity);
		
		//Set the actionbar background
		mActionBar = getActionBar();
		Drawable actionBarBg = getResources().getDrawable(R.drawable.ab_background_blue);
        mActionBar.setBackgroundDrawable(actionBarBg);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
        	actionBarBg.setCallback(mDrawableCallback);
        }
        
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setTitle(R.string.Fuel_ups_summed);

		mVehicleId = getIntent().getLongExtra(VEHICLE_ID, -1);
		mNumberFormat = NumberFormat.getInstance();
		
		updateView();
	}

	/*
	 * Begin of FadingActionBar shizzle
	 */
    private Drawable.Callback mDrawableCallback = new Drawable.Callback() {
        @Override
        public void invalidateDrawable(Drawable who) {
            mActionBar.setBackgroundDrawable(who);
        }

        @Override
        public void scheduleDrawable(Drawable who, Runnable what, long when) {
        }

        @Override
        public void unscheduleDrawable(Drawable who, Runnable what) {
        }
    };
    /*
     * End of FadingActionBar shizzle
     */

	private void updateView() {
		ViewGroup container = (ViewGroup) findViewById(R.id.vehicleInfoContainer);
//		container.addView(totalsView);
		getTotalsView(container);
	}
    
    @SuppressLint("DefaultLocale")
	private View getTotalsView(ViewGroup container) {

		LayoutInflater inflater = getLayoutInflater();
		View view = inflater.inflate(R.layout.vehicle_totals_info, container);
    	
    	if (mVehicleId < 0) {
			return view;
		}
    	
		FuelUpAveragesDb averagesDb = new FuelUpAveragesDb(getApplicationContext());
		averagesDb.open();
		FuelUpAverages fuelUpAverages = averagesDb.getFuelUpAverageByVehicleId(mVehicleId);
		averagesDb.close();
		
		//Total fuel-ups
		setTotalValue(view, 
				R.id.totalFuelUpsTitle, 
				R.string.Number_of_fuel_ups, 
				R.id.totalFuelUpsText, 
				fuelUpAverages.getTotalFuelUps());
		
		//Total distance tracked
		long totalDistanceShown = Math.round(fuelUpAverages.getTotalDistance());
		setTotalValue(view, 
				R.id.totalDistanceTitle, 
				R.string.Distance_tracked, 
				R.id.totalDistanceText, 
				totalDistanceShown, 
				R.id.totalDistanceSubtitle, 
				R.string.km);
		
		//Total fuel consumed
		long totalFuelConsumedShown = Math.round(fuelUpAverages.getTotalFuelConsumed());
		setTotalValue(view, 
				R.id.totalConsumedTitle, 
				R.string.Fuel_consumed, 
				R.id.totalConsumedText, 
				totalFuelConsumedShown, 
				R.id.totalConsumedSubtitle, 
				R.string.litres);
		
		//Total money spent
		long totalMoneySpentShown = Math.round(fuelUpAverages.getTotalMoneySpent());
		setTotalValue(view, 
				R.id.totalSpentTitle, 
				R.string.Money_spent, 
				R.id.totalSpentText, 
				totalMoneySpentShown, 
				R.id.totalSpentSubtitle, 
				R.string.euro);
		
		//By enabling the 2 lines below bugs will appear in the onClickListener of the other rows with the parallax effect
//		view.setOnClickListener(null);
//		view.setEnabled(false);
		
		return view;
    }
	
	/*
	 * START: private methods to fill in the values of the rows
	 */
	
	private void setTotalValue(View view, int titleViewId, int titleId, int subjectViewId, long subject) {
		setTotalValue(view, titleViewId, titleId, subjectViewId, subject, -1, -1);
	}
	
	private void setTotalValue(View view, int titleViewId, int titleId, int subjectViewId, long subject, int subtitleViewId, int subtitleId) {
		long subjectShown = Math.round(subject);
		String subjectString = mNumberFormat.format(subjectShown);
		setRowValue(view, titleViewId, titleId, subjectViewId, subjectString, subtitleViewId, subtitleId);
	}
	
	@SuppressLint("DefaultLocale")
	private void setRowValue(View view, int titleViewId, int titleId, int subjectViewId, String subject, int subtitleViewId, int subtitleId) {
		String subtitle = "";
		if (subtitleId >= 0) {
			subtitle = getString(subtitleId).toUpperCase();
		}
		setRowValue(view, titleViewId, titleId, subjectViewId, subject, subtitleViewId, subtitle);
	}
    
    @SuppressLint("DefaultLocale")
	private void setRowValue(View view, int titleViewId, int titleId, int subjectViewId, String subject, int subtitleViewId, String subtitle) {
		
		if (titleViewId >= 0 && titleId >= 0) {
			//Title
			TextView titleView = (TextView) view.findViewById(titleViewId);
			String title = getString(titleId).toUpperCase();
			titleView.setText(title);
		}
		
		if (subjectViewId >= 0) {
			//Subject
			TextView subjectView = (TextView) view.findViewById(subjectViewId);
			subjectView.setText(subject);
		}
		if (subtitleViewId >= 0) {
			//Subtitle
			TextView subTitleView = (TextView) view.findViewById(subtitleViewId);
			subTitleView.setText(subtitle);
		}
    }
    
    /*
     * END
     *
     */
}
