package com.bithyve.android.fuelapp.vehicles.edit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.Vehicle;
import com.bithyve.android.fuelapp.data.VehiclesDb;
import com.bithyve.android.fuelapp.menu.MenuListFragment;
import com.bithyve.android.fuelapp.scrollview.NotifyingParallaxScrollView;

import eu.janmuller.android.simplecropimage.CropImage;

//TODO: show a back arrow in the actionbar
public class EditVehicleDetailsActivity extends Activity {
	
	public static final String IS_NEW = "isNew";
	public static final String VEHICLE_ID = "vehicleId";
	
	private final int SELECT_IMAGE = 100;
	private final int CROP_IMAGE = 101;
    private final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
	
	private boolean isNew;
	private long vehicleId;
	private Drawable vehicleImage;
	private boolean newImage;
	private String errorMessage;
	
	private ActionBar mActionBar;
	private ImageView mHeaderView;
	private Drawable mActionBarBackgroundDrawable;
	
	private File mFileTemp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_vehicle_details_activity);
		
		isNew = getIntent().getBooleanExtra(IS_NEW, false);
		
		//Set the actionbar home button
		mActionBar = getActionBar();
		mActionBar.setHomeButtonEnabled(true);
		
		//Set the actionbar background
		mActionBarBackgroundDrawable = getResources().getDrawable(R.drawable.ab_background_blue);
        mActionBar.setBackgroundDrawable(mActionBarBackgroundDrawable);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            mActionBarBackgroundDrawable.setCallback(mDrawableCallback);
        }
        mActionBarBackgroundDrawable.setAlpha(0);
		
		mHeaderView = (ImageView)findViewById(R.id.imageHeader);
		
		NotifyingParallaxScrollView scrollView = (NotifyingParallaxScrollView) findViewById(R.id.vehicleDetailsContainer);
        scrollView.setOnScrollChangedListener(mOnScrollChangedListener);
		
		if (isNew) {
			mActionBar.setTitle(R.string.Add_vehicle);
		} else {
			mActionBar.setTitle(R.string.Edit_vehicle);
			//Try to get the vehicle, otherwise we should exit this activity
			vehicleId = getIntent().getLongExtra(VEHICLE_ID, -1);
			VehiclesDb db = new VehiclesDb(getApplicationContext());
			db.open();
			Vehicle vehicle = db.getVehicleById(vehicleId);
			db.close();
			if (vehicle == null) {
				setResult(RESULT_CANCELED);
	    		finish();
			} else {
				setVehicleDetails(vehicle);
			}
		}
		
		initPhotoPicker();
//		setUnitPreferencesRows();
	}

	/*
	 * Begin of FadingActionBar shizzle
	 */
    private Drawable.Callback mDrawableCallback = new Drawable.Callback() {
        @Override
        public void invalidateDrawable(Drawable who) {
            mActionBar.setBackgroundDrawable(who);
        }

        @Override
        public void scheduleDrawable(Drawable who, Runnable what, long when) {
        }

        @Override
        public void unscheduleDrawable(Drawable who, Runnable what) {
        }
    };

    private NotifyingParallaxScrollView.OnScrollChangedListener mOnScrollChangedListener = new NotifyingParallaxScrollView.OnScrollChangedListener() {
        public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
            onNewScroll(t);
        }
    };

    private void onNewScroll(int scrollPosition) {
        if (mActionBar == null) {
            return;
        }

        int currentHeaderHeight = mHeaderView.getHeight();
        int headerHeight = currentHeaderHeight - mActionBar.getHeight();
        float ratio = (float) Math.min(Math.max(scrollPosition, 0), headerHeight) / headerHeight;
        int newAlpha = (int) (ratio * 255);
        mActionBarBackgroundDrawable.setAlpha(newAlpha);
    }
    /*
     * End of FadingActionBar shizzle
     */
    
    

	/*
	 * The method below is saved for possible future purposes.
	 * 
	 * TODO: Override saving the preferences to the GlobalPreferences,
	 * instead these should be saved at the vehicle entity.
	 */
    /*
    private void setUnitPreferencesRows() {
    	
    	LayoutInflater inflater = LayoutInflater.from(this);
    	
		//Global vehicle prefences
    	ViewGroup vehiclePrefContainer = (ViewGroup) findViewById(R.id.vehiclePrefContainer);
    	
    	//Track meter unit
    	TrackMeterRow trackMeterRow = new TrackMeterRow(getFragmentManager(), this);
    	View trackMeterUnitRow = trackMeterRow.getRowView(inflater);
    	vehiclePrefContainer.addView(trackMeterUnitRow);
    	
    	//Distance unit
    	DistanceRow distanceRow = new DistanceRow(getFragmentManager(), this);
    	View distanceUnitRow = distanceRow.getRowView(inflater);
    	vehiclePrefContainer.addView(distanceUnitRow);
    	
    	//Efficiency notation
    	EfficiencyRow efficiencyRow = new EfficiencyRow(getFragmentManager(), this);
    	View efficiencyNotationRow = efficiencyRow.getRowView(inflater);
    	vehiclePrefContainer.addView(efficiencyNotationRow);
    }*/
    
    private void initPhotoPicker() {
    	mHeaderView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		    	photoPickerIntent.setType("image/*");
		    	startActivityForResult(photoPickerIntent, SELECT_IMAGE);
			}
		});
    }
	
	private void setVehicleDetails(Vehicle vehicle) {
		//Name
		EditText nameText = (EditText) findViewById(R.id.vehicleName);
		nameText.setText(vehicle.getName());
		
		//Make
		EditText makeText = (EditText) findViewById(R.id.vehicleMake);
		makeText.setText(vehicle.getMake());
		
		//Model
		EditText modelText = (EditText) findViewById(R.id.vehicleModel);
		modelText.setText(vehicle.getModel());
		
		//Year
		if (vehicle.getYear() > 0) {
			EditText yearText = (EditText) findViewById(R.id.vehicleYear);
			String yearString = Integer.toString(vehicle.getYear());
			yearText.setText(yearString);
		}
		
		//Fuel type
		EditText fuelText = (EditText) findViewById(R.id.vehicleFuelType);
		fuelText.setText(vehicle.getFuelType());
		
		//Notes
		EditText notesText = (EditText) findViewById(R.id.vehicleNotes);
		notesText.setText(vehicle.getNotes());
		
		//Image
		vehicleImage = vehicle.getImage();
		if (vehicleImage != null) {
			mHeaderView.setImageDrawable(vehicleImage);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.done_item, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean itemSelected;
		
		switch (item.getItemId()) {
		case android.R.id.home:
			setResult(RESULT_CANCELED);
    		finish();
			itemSelected = true;
			break;
		case R.id.doneItem:
			showProgressAndStoreVehicle();
			itemSelected = true;
			break;

		default:
			itemSelected = super.onOptionsItemSelected(item);
			break;
		}
		
		return itemSelected;
	}
	
	private void showProgressAndStoreVehicle() {
		new AsyncTask<Void, Void, List<Vehicle>>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				
				//Hide keyboard
				EditText nameText = (EditText) findViewById(R.id.vehicleName);
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(nameText.getWindowToken(), 0);
				
				//Show activity indicator
				View progressBarContainer = findViewById(R.id.progressBarContainer);
				progressBarContainer.setVisibility(View.VISIBLE);
			}

			@Override
			protected List<Vehicle> doInBackground(Void... params) {
				List<Vehicle> vehicles = null;
				boolean success = storeVehicleInfo();
				if (success) {
					VehiclesDb db = new VehiclesDb(getApplicationContext());
					db.open();
					vehicles = db.getAllVehicles();
					db.close();
				}
				
				return vehicles;
			}

			@Override
			protected void onPostExecute(List<Vehicle> vehicles) {
				super.onPostExecute(vehicles);
				
				if (vehicles != null) {
					//A vehicle was added or modified, so we should update the menu
					MenuListFragment menuList = MenuListFragment.getInstance();
					if (menuList != null) {
						menuList.updateMenu(EditVehicleDetailsActivity.this, vehicles);
					}

					setResult(RESULT_OK);
		    		finish();
					
				} else {
					View progressBarContainer = findViewById(R.id.progressBarContainer);
					progressBarContainer.setVisibility(View.GONE);
					
					if (errorMessage.length() <= 0) {
						errorMessage = getString(R.string.ErrorStoringVehicle);
					}
					//Show error message
					AlertDialog.Builder builder = new AlertDialog.Builder(EditVehicleDetailsActivity.this);
					builder.setMessage(errorMessage)
						   .setTitle("Input error")
						   .setPositiveButton(android.R.string.ok, null)
						   .setCancelable(true);
					builder.create().show();
				}
			}
			
		}.execute();
	}
	
	private boolean storeVehicleInfo() {
//		ID, NAME, MAKE, MODEL, YEAR, FUEL_TYPE, NOTES, IMAGE
		String name, make, model, fuelType, notes;
		int year = -1;
		boolean valid = true;
		StringBuilder error = new StringBuilder();
		
		//Name
		EditText nameText = (EditText) findViewById(R.id.vehicleName);
		name = nameText.getText().toString();
		
		if (name.length() < 1) {
			valid = false;
			error.append(getString(R.string.ErrorVehicleNoName));
		}
		
		//Make
		EditText makeText = (EditText) findViewById(R.id.vehicleMake);
		make = makeText.getText().toString();
		
		//Model
		EditText modelText = (EditText) findViewById(R.id.vehicleModel);
		model = modelText.getText().toString();
		
		//Year
		EditText yearText = (EditText) findViewById(R.id.vehicleYear);
		String yearString = yearText.getText().toString();
		
		if (yearString.length() > 0) {
			boolean yearIsANumber = false;
			try {
				year = Integer.parseInt(yearString);
				yearIsANumber = true;
			} catch (NumberFormatException e) {
				e.printStackTrace();
				valid = false;
				
				if (error.length() > 0) {
					error.append("\n");
				}
				error.append(getString(R.string.ErrorYearNaN));
			}
			
			Calendar calendar = Calendar.getInstance();
			int currentYear = calendar.get(Calendar.YEAR);
			if (yearIsANumber) {
				if (year > (currentYear + 1)) {
					valid = false;
					
					if (error.length() > 0) {
						error.append("\n");
					}
					error.append(getString(R.string.ErrorYearInFuture));
				} else if(year < 1886) {
					valid = false;
					
					if (error.length() > 0) {
						error.append("\n");
					}
					error.append(getString(R.string.ErrorYearInPast));
				}
			}
		}
		
		
		//Fuel type
		EditText fuelText = (EditText) findViewById(R.id.vehicleFuelType);
		fuelType = fuelText.getText().toString();
		
		//Notes
		EditText notesText = (EditText) findViewById(R.id.vehicleNotes);
		notes = notesText.getText().toString();
		
		if (valid) {
			//Store the vehicle in the db
			Vehicle vehicle = new Vehicle(name);
			vehicle.setMake(make);
			vehicle.setModel(model);
			vehicle.setYear(year);
			vehicle.setFuelType(fuelType);
			vehicle.setNotes(notes);
			
			if (vehicleImage != null) {
				vehicle.setImage(vehicleImage);
			}
			
			if (!isNew) {
				vehicle.setId(vehicleId);
			}
			
			VehiclesDb db = new VehiclesDb(getApplicationContext());
			db.open();
			long rowID = db.storeVehicle(vehicle, newImage);
			db.close();
			
			//rowID is -1 when there's a failure
			valid = rowID >= 0;
			
		} else {
			//Set the error message
			errorMessage = error.toString();
		}
		
		return valid;
	}

    private void startCropImage() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 2);

        startActivityForResult(intent, CROP_IMAGE);
    }

    private void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode != RESULT_OK) {
			return;
		}
		
		switch(requestCode) { 
	    case SELECT_IMAGE:
            try {
            	String state = Environment.getExternalStorageState();
            	if (Environment.MEDIA_MOUNTED.equals(state)) {
            		mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
            	}
            	else {
            		mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
            	}
            	
            	InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
    		
            startCropImage();
            break;

        case CROP_IMAGE:

            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {

                return;
            }

            Bitmap bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
    		Drawable image = new BitmapDrawable(getResources(), bitmap);
    		if (image != null) {
    			vehicleImage = image;
    			mHeaderView.setImageDrawable(vehicleImage);
    			newImage = true;
			}
    		
            break;
	    }
	}

}
