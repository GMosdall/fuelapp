package com.bithyve.android.fuelapp.vehicles.edit;

import java.util.List;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.Vehicle;
import com.bithyve.android.fuelapp.data.VehiclesDb;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListFragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

//TODO: implement delete vehicle (by sliding the row)
public class EditVehiclesFragment extends ListFragment {

	private final int EDIT_VEHICLE_DETAILS = 1337;
	private ActionBar mActionBar;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View editVehicles = inflater.inflate(R.layout.edit_vehicles_fragment, null);
		
		//Set the actionbar background
		mActionBar = getActivity().getActionBar();
		mActionBar.setTitle(R.string.Edit_vehicles);
		Drawable actionBarBg = getResources().getDrawable(R.drawable.ab_background_blue);
        mActionBar.setBackgroundDrawable(actionBarBg);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
        	actionBarBg.setCallback(mDrawableCallback);
        }
		
		//The options are set in onCreateOptionsMenu
		setHasOptionsMenu(true);
		
		return editVehicles;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//TODO: Research - Why is this here and not in onCreateView()?
		this.populateVehicleList();
	}

	/*
	 * Begin of FadingActionBar shizzle
	 */
    private Drawable.Callback mDrawableCallback = new Drawable.Callback() {
        @Override
        public void invalidateDrawable(Drawable who) {
            mActionBar.setBackgroundDrawable(who);
        }

        @Override
        public void scheduleDrawable(Drawable who, Runnable what, long when) {
        }

        @Override
        public void unscheduleDrawable(Drawable who, Runnable what) {
        }
    };
    /*
     * End of FadingActionBar shizzle
     */
	
	private void populateVehicleList() {
		VehicleAdapter adapter = new VehicleAdapter(getActivity().getApplicationContext());
		
		VehiclesDb db = new VehiclesDb(getActivity().getApplicationContext());
		db.open();
		List<Vehicle> vehicles = db.getAllVehicles();
		db.close();
		
		for (Vehicle vehicle : vehicles) {
			View vehicleRow = getListItemView(
					R.layout.edit_vehicles_row, 
					vehicle.getImage(), 
					vehicle.getName(), 
					true);
			vehicleRow.setTag(R.id.vehicleIdTag, vehicle.getId());
			adapter.add(new VehicleListItem(vehicleRow));
		}
		
		setListAdapter(adapter);
	}
	
	private View getListItemView(int layoutId, Drawable vehicleImage, String titleString, boolean clickable) {
//    	LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		View menuItem = inflater.inflate(layoutId, null);
		//This seems to work faster than the possibility above
		View menuItem = LayoutInflater.from(getActivity()).inflate(layoutId, null);
		
		ImageView imageView = (ImageView) menuItem.findViewById(R.id.rowVehicleImage);
		if (imageView != null && vehicleImage != null) {
			imageView.setImageDrawable(vehicleImage);
		}
		
		TextView title = (TextView) menuItem.findViewById(R.id.rowTitle);
		if (title != null && titleString != null) {
			title.setText(titleString);
		}
		
		if (!clickable) {
			menuItem.setOnClickListener(null);
			menuItem.setEnabled(false);
		}
		
		return menuItem;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		long vehicleId = (Long) v.getTag(R.id.vehicleIdTag);
		
		Intent intent = new Intent(getActivity(), EditVehicleDetailsActivity.class);
		intent.putExtra(EditVehicleDetailsActivity.IS_NEW, false);
		intent.putExtra(EditVehicleDetailsActivity.VEHICLE_ID, vehicleId);
		startActivityForResult(intent, EDIT_VEHICLE_DETAILS);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add_vehicle_item, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean itemSelected;
		
		switch (item.getItemId()) {
		case R.id.addVehicleItem:
    		Intent intent = new Intent(getActivity(), EditVehicleDetailsActivity.class);
    		intent.putExtra(EditVehicleDetailsActivity.IS_NEW, true);
    		startActivityForResult(intent, EDIT_VEHICLE_DETAILS);
    		
			itemSelected = true;
			break;

		default:
			itemSelected = super.onOptionsItemSelected(item);
			break;
		}
		
		return itemSelected;
	}

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_VEHICLE_DETAILS) {
            if (resultCode == Activity.RESULT_OK) {
        		this.populateVehicleList();
            }
        }
    }

}
