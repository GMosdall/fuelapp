package com.bithyve.android.fuelapp.vehicles.edit;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

class VehicleAdapter extends ArrayAdapter<VehicleListItem> {
	
	VehicleAdapter(Context context) {
		super(context, 0);
	}

	public View getView(int position, View rowView, ViewGroup parent) {
		VehicleListItem item = getItem(position);
		return item.view;
	}
}
