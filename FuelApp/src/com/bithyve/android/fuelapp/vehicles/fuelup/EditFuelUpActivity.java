package com.bithyve.android.fuelapp.vehicles.fuelup;


import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.custom.DatePickerFragment;
import com.bithyve.android.fuelapp.data.FuelUp;
import com.bithyve.android.fuelapp.data.FuelUpsDb;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class EditFuelUpActivity extends Activity implements OnDateSetListener {
	
	public static final String FUEL_UP_ID = "fuelUpId";
	public static final String VEHICLE_ID = "vehicleId";
	public static final String PREVIOUS_FUEL_UP_ID = "previousFuelUpId";
	
	private ActionBar mActionBar;
	private long mFuelUpId;
	private long mVehicleId;
	private Date mFuelUpDate;
	private long mPreviousFuelUpId;
	private FuelUp mOldFuelUp;
	private String mErrorMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_fuel_up_activity);
		
		//Set the actionbar background
		mActionBar = getActionBar();
		Drawable actionBarBg = getResources().getDrawable(R.drawable.ab_background_blue);
        mActionBar.setBackgroundDrawable(actionBarBg);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
        	actionBarBg.setCallback(mDrawableCallback);
        }
        
		mActionBar.setHomeButtonEnabled(true);
		
		mPreviousFuelUpId = -1;
		mFuelUpId = getIntent().getLongExtra(FUEL_UP_ID, -1);
		if (mFuelUpId <= 0) {
			mVehicleId = getIntent().getLongExtra(VEHICLE_ID, -1);
			if (mVehicleId <= 0) {
				setResult(RESULT_CANCELED);
	    		finish();
			}

			mActionBar.setTitle(R.string.Add_fuel_up);
			updateDateView();
		} else {
			mActionBar.setTitle(R.string.Edit_fuel_up);
			//Try to get the fuel up, otherwise we should exit this activity
			FuelUpsDb db = new FuelUpsDb(getApplicationContext());
			db.open();
			mOldFuelUp = db.getFuelUpById(mFuelUpId);
			db.close();
			if (mOldFuelUp == null) {
				setResult(RESULT_CANCELED);
	    		finish();
			} else {
				mVehicleId = mOldFuelUp.getVehicleId();
				mFuelUpDate = mOldFuelUp.getDate();
				mPreviousFuelUpId = getIntent().getLongExtra(PREVIOUS_FUEL_UP_ID, -1);
			}
			setFuelUpDetails(mOldFuelUp);
		}
		
		initDateTextView();
	}

	/*
	 * Begin of FadingActionBar shizzle
	 */
    private Drawable.Callback mDrawableCallback = new Drawable.Callback() {
        @Override
        public void invalidateDrawable(Drawable who) {
            mActionBar.setBackgroundDrawable(who);
        }

        @Override
        public void scheduleDrawable(Drawable who, Runnable what, long when) {
        }

        @Override
        public void unscheduleDrawable(Drawable who, Runnable what) {
        }
    };
    /*
     * End of FadingActionBar shizzle
     */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.done_item, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean itemSelected;
		
		switch (item.getItemId()) {
		case android.R.id.home:
			setResult(RESULT_CANCELED);
    		finish();
			itemSelected = true;
			break;
		case R.id.doneItem:
			showProgressAndStoreFuelUp();
			itemSelected = true;
			break;

		default:
			itemSelected = super.onOptionsItemSelected(item);
			break;
		}
		
		return itemSelected;
	}
	
	private void initDateTextView() {
		//Set the listener to input the date
		TextView dateTv = (TextView) findViewById(R.id.fuelUpDate);
		dateTv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showDatePicker();
			}
		});
	}
	
	private void showDatePicker() {
		Bundle bundle = new Bundle();
		bundle.putSerializable(DatePickerFragment.DATE, this.mFuelUpDate);
		DialogFragment dateFragment = new DatePickerFragment();
		dateFragment.setArguments(bundle);
		
		dateFragment.show(getFragmentManager(), "datePicker");
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar cal = new GregorianCalendar(year, monthOfYear, dayOfMonth);
		mFuelUpDate = cal.getTime();
		updateDateView();
	}
	
	private String getDateString() {
		if (mFuelUpDate == null) {
			mFuelUpDate = new Date();
		}
		DateFormat dateFormat = android.text.format.DateFormat.getMediumDateFormat(this);
		String dateString = dateFormat.format(mFuelUpDate);
		return dateString;
	}
	
	private void updateDateView() {
		String dateString = getDateString();
		TextView dateTv = (TextView) findViewById(R.id.fuelUpDate);
		dateTv.setText(dateString);
	}
	
	private void setFuelUpDetails(FuelUp fuelUp) {
		//Distance
		EditText distanceText = (EditText) findViewById(R.id.fuelUpDistance);
		String distanceString = String.valueOf(fuelUp.getDistance());
		distanceText.setText(distanceString);
		
		//Amount
		EditText amountText = (EditText) findViewById(R.id.fuelUpAmount);
		String amountString = String.valueOf(fuelUp.getAmount());
		amountText.setText(amountString);
		
		//Price
		EditText priceText = (EditText) findViewById(R.id.fuelUpPrice);
		String priceString = String.valueOf(fuelUp.getPrice());
		priceText.setText(priceString);
		
		//Date
		String dateString = getDateString();
		TextView dateTv = (TextView) findViewById(R.id.fuelUpDate);
		dateTv.setText(dateString);
		
		//Notes
		EditText notesText = (EditText) findViewById(R.id.fuelUpNotes);
		String notesString = String.valueOf(fuelUp.getNotes());
		notesText.setText(notesString);
	}
	
	private void showProgressAndStoreFuelUp() {
		new AsyncTask<Void, Void, Boolean>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				
				//Hide keyboard
				EditText nameText = (EditText) findViewById(R.id.fuelUpDistance);
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(nameText.getWindowToken(), 0);
				
				//Show activity indicator
				View progressBarContainer = findViewById(R.id.progressBarContainer);
				progressBarContainer.setVisibility(View.VISIBLE);
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				boolean success = storeFuelUpInfo();
				return success;
			}

			@Override
			protected void onPostExecute(Boolean success) {
				super.onPostExecute(success);
				
				if (success) {
					//A fuel up was succesfully added or modified
					setResult(RESULT_OK);
		    		finish();
					
				} else {
					View progressBarContainer = findViewById(R.id.progressBarContainer);
					progressBarContainer.setVisibility(View.GONE);
					
					if (mErrorMessage.length() <= 0) {
						mErrorMessage = getString(R.string.ErrorStoringFuelUp);
					}
					//Show error message
					AlertDialog.Builder builder = new AlertDialog.Builder(EditFuelUpActivity.this);
					builder.setMessage(mErrorMessage)
						   .setTitle("Input error")
						   .setPositiveButton(android.R.string.ok, null)
						   .setCancelable(true);
					builder.create().show();
				}
			}
			
		}.execute();
	}
	
	private boolean storeFuelUpInfo() {
//		ID, VEHICLE_ID, DISTANCE, AMOUNT, PRICE, DATE, NOTES
		double distance = -1;
		double amount = -1;
		double price = -1;
		String notes;
		
		boolean valid = true;
		StringBuilder error = new StringBuilder();
		
		//Distance
		EditText distanceText = (EditText) findViewById(R.id.fuelUpDistance);
		String distanceString = distanceText.getText().toString();
		try {
			distance = Double.parseDouble(distanceString);
			if (distance <= 0) {
				throw new NumberFormatException("Distance is not a positive number");
			}
		} catch (NumberFormatException e1) {
			valid = false;
			error.append(getString(R.string.ErrorDistanceNaN));
			e1.printStackTrace();
		}
		
		//Amount
		EditText amountText = (EditText) findViewById(R.id.fuelUpAmount);
		String amountString = amountText.getText().toString();
		try {
			amount = Double.parseDouble(amountString);
			if (amount <= 0) {
				throw new NumberFormatException("Amount is not a positive number");
			}
		} catch (NumberFormatException e1) {
			valid = false;
			if (error.length() > 0) {
				error.append("\n");
			}
			error.append(getString(R.string.ErrorAmountNaN));
			e1.printStackTrace();
		}
		
		//Price
		EditText priceText = (EditText) findViewById(R.id.fuelUpPrice);
		String priceString = priceText.getText().toString();
		try {
			price = Double.parseDouble(priceString);
			if (amount <= 0) {
				throw new NumberFormatException("Price is not a positive number");
			}
		} catch (NumberFormatException e1) {
			valid = false;
			if (error.length() > 0) {
				error.append("\n");
			}
			error.append(getString(R.string.ErrorPriceNaN));
			e1.printStackTrace();
		}
		
		//Notes
		EditText notesText = (EditText) findViewById(R.id.fuelUpNotes);
		notes = notesText.getText().toString();
		
		if (valid) {
			//Store the fuel up in the db
			FuelUp fuelUp = new FuelUp(mVehicleId);
			fuelUp.setId(mFuelUpId);
			fuelUp.setVehicleId(mVehicleId);
			fuelUp.setDate(mFuelUpDate);
			fuelUp.setDistance(distance);
			fuelUp.setAmount(amount);
			fuelUp.setPrice(price);
			fuelUp.setNotes(notes);
			
			FuelUpsDb db = new FuelUpsDb(getApplicationContext());
			db.open();
			long rowID = db.storeFuelUp(fuelUp, mOldFuelUp, mPreviousFuelUpId);
			db.close();
			
			//rowID is -1 when there's a failure and SQLite rows start with 1
			valid = rowID > 0;
			
		} else {
			//Set the error message
			mErrorMessage = error.toString();
		}
		
		return valid;
	}

}
