package com.bithyve.android.fuelapp.vehicles.fuelup;

import java.text.NumberFormat;
import java.util.List;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.FuelUp;
import com.bithyve.android.fuelapp.data.FuelUpAverages;
import com.bithyve.android.fuelapp.data.FuelUpAveragesDb;
import com.bithyve.android.fuelapp.data.FuelUpsDb;
import com.bithyve.android.fuelapp.utils.DateUtil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class FuelUpsOverview extends ListActivity {

	public static final String VEHICLE_ID = "vehicleId";
	private final int EDIT_FUEL_UP = 1000;

	private ActionBar mActionBar;
	private long mRowNumber;
	private long mVehicleId;
	private int mResultCode;
	
	private boolean mDarkBackground;
	private NumberFormat mNumberFormat;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fuel_ups_overview_activity);
		mResultCode = RESULT_CANCELED;
		
		//Set the actionbar background
		mActionBar = getActionBar();
		Drawable actionBarBg = getResources().getDrawable(R.drawable.ab_background_blue);
        mActionBar.setBackgroundDrawable(actionBarBg);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
        	actionBarBg.setCallback(mDrawableCallback);
        }
        
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setTitle(R.string.Fuel_ups_overview);
		
		mVehicleId = getIntent().getLongExtra(VEHICLE_ID, -1);
		mNumberFormat = NumberFormat.getInstance();
		
		this.populateFuelUpList();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		//Start bright again :)
		mDarkBackground = false;
	}

	private void populateFuelUpList() {
		FuelUpAdapter adapter = new FuelUpAdapter(getApplicationContext());
		
		//Set the totals row
		View totalsRow = getTotalsListItemView();
		totalsRow.setTag(R.id.fuelUpIdTag, -1);
		adapter.add(new FuelUpListItem(totalsRow));
		
		//Fetch all fuel-ups
		FuelUpsDb db = new FuelUpsDb(getApplicationContext());
		db.open();
		List<FuelUp> fuelUps = db.getAllFuelUpsByVehicle(mVehicleId);
		db.close();
		
		mRowNumber = fuelUps.size();
		
		for (int i = fuelUps.size() - 1; i >= 0; i--) {
			FuelUp fuelUp = fuelUps.get(i);
			View row = getListItemView(fuelUp);
			row.setTag(R.id.fuelUpIdTag, fuelUp.getId());
			adapter.add(new FuelUpListItem(row));
		}
		
		setListAdapter(adapter);
	}

	/*
	 * Begin of FadingActionBar shizzle
	 */
    private Drawable.Callback mDrawableCallback = new Drawable.Callback() {
        @Override
        public void invalidateDrawable(Drawable who) {
            mActionBar.setBackgroundDrawable(who);
        }

        @Override
        public void scheduleDrawable(Drawable who, Runnable what, long when) {
        }

        @Override
        public void unscheduleDrawable(Drawable who, Runnable what) {
        }
    };
    /*
     * End of FadingActionBar shizzle
     */
    
    @SuppressLint("DefaultLocale")
	private View getTotalsListItemView() {
		FuelUpAveragesDb averagesDb = new FuelUpAveragesDb(getApplicationContext());
		averagesDb.open();
		FuelUpAverages fuelUpAverages = averagesDb.getFuelUpAverageByVehicleId(mVehicleId);
		averagesDb.close();

		LayoutInflater inflater = getLayoutInflater();
		View view = inflater.inflate(R.layout.vehicle_totals_info, null);
		
		//Total fuel-ups
		setTotalValue(view, 
				R.id.totalFuelUpsTitle, 
				R.string.Number_of_fuel_ups, 
				R.id.totalFuelUpsText, 
				fuelUpAverages.getTotalFuelUps());
		
		//Total distance tracked
		long totalDistanceShown = Math.round(fuelUpAverages.getTotalDistance());
		setTotalValue(view, 
				R.id.totalDistanceTitle, 
				R.string.Distance_tracked, 
				R.id.totalDistanceText, 
				totalDistanceShown, 
				R.id.totalDistanceSubtitle, 
				R.string.km);
		
		//Total fuel consumed
		long totalFuelConsumedShown = Math.round(fuelUpAverages.getTotalFuelConsumed());
		setTotalValue(view, 
				R.id.totalConsumedTitle, 
				R.string.Fuel_consumed, 
				R.id.totalConsumedText, 
				totalFuelConsumedShown, 
				R.id.totalConsumedSubtitle, 
				R.string.litres);
		
		//Total money spent
		long totalMoneySpentShown = Math.round(fuelUpAverages.getTotalMoneySpent());
		setTotalValue(view, 
				R.id.totalSpentTitle, 
				R.string.Money_spent, 
				R.id.totalSpentText, 
				totalMoneySpentShown, 
				R.id.totalSpentSubtitle, 
				R.string.euro);
		
		//By enabling the 2 lines below bugs will appear in the onClickListener of the other rows with the parallax effect
//		view.setOnClickListener(null);
//		view.setEnabled(false);
		
		return view;
    }
    
	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressLint("DefaultLocale")
	private View getListItemView(FuelUp fuelUp) {
//    	LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		View menuItem = inflater.inflate(layoutId, null);
		//This seems to work faster than the possibility above
		LayoutInflater inflater = getLayoutInflater();
		View view = inflater.inflate(R.layout.fuel_ups_overview_row, null);
		
		//Fuel up number
		String numberString = String.valueOf(mRowNumber);
		mRowNumber--;
		setRowValue(view, 
				R.id.rowNumberTitle, 
				R.string.Fuel_up, 
				R.id.rowNumber, 
				numberString);
		
		//Date
		String dayMonthString = DateUtil.getMediumDayMonthString(fuelUp.getDate());
		String yearString = DateUtil.getYearString(fuelUp.getDate());
		setRowValue(view, 
				R.id.rowDateTitle, 
				R.string.Date, 
				R.id.rowDate, 
				dayMonthString, 
				R.id.rowDateSubtitle, 
				yearString);
		
		//Efficiency
		double efficiency = fuelUp.getDistance() / fuelUp.getAmount();
		String effString = String.format("%.2f", efficiency);
		setRowValue(view, 
				R.id.rowEfficiencyTitle, 
				R.string.Efficiency, 
				R.id.rowEfficiency, 
				effString, 
				R.id.rowEfficiencySubtitle, 
				R.string.Kilometres_per_litre_abb);
		
		//Second row
		//Distance
		String distance = String.format("%.2f", fuelUp.getDistance());
		setRowValue(view, 
				R.id.rowDistanceTitle, 
				R.string.Distance, 
				R.id.rowDistance, 
				distance, 
				R.id.rowDistanceSubtitle, 
				R.string.km);
		
		//Amount / Consumed
		String consumed = String.format("%.2f", fuelUp.getAmount());
		setRowValue(view, 
				R.id.rowConsumedTitle, 
				R.string.Consumed, 
				R.id.rowConsumed, 
				consumed, 
				R.id.rowConsumedSubtitle, 
				R.string.litres);
		
		//Price
		String price = String.format("%.3f", fuelUp.getPrice());
		setRowValue(view, 
				R.id.rowPriceTitle, 
				R.string.Price, 
				R.id.rowPrice, 
				price, 
				R.id.rowPriceSubtitle, 
				R.string.euro_L);
		
		//Define background color
		Drawable backgroundSelector;
		if (mDarkBackground) {
			backgroundSelector = getResources().getDrawable(R.drawable.fuel_ups_overview_row_dark_selector);
		} else {
			backgroundSelector = getResources().getDrawable(R.drawable.fuel_ups_overview_row_light_selector);
		}
		

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
			view.setBackground(backgroundSelector);
		} else {
			//TODO: Remove when dropping ICS support
			view.setBackgroundDrawable(backgroundSelector);
		}
		
		mDarkBackground = !mDarkBackground;
		
		return view;
	}
	
	/*
	 * START: private methods to fill in the values of the rows
	 */
	
	private void setTotalValue(View view, int titleViewId, int titleId, int subjectViewId, long subject) {
		setTotalValue(view, titleViewId, titleId, subjectViewId, subject, -1, -1);
	}
	
	private void setTotalValue(View view, int titleViewId, int titleId, int subjectViewId, long subject, int subtitleViewId, int subtitleId) {
		long subjectShown = Math.round(subject);
		String subjectString = mNumberFormat.format(subjectShown);
		setRowValue(view, titleViewId, titleId, subjectViewId, subjectString, subtitleViewId, subtitleId);
	}
	
	private void setRowValue(View view, int titleViewId, int titleId, int subjectViewId, String subject) {
		setRowValue(view, titleViewId, titleId, subjectViewId, subject, -1, -1);
	}
	
	@SuppressLint("DefaultLocale")
	private void setRowValue(View view, int titleViewId, int titleId, int subjectViewId, String subject, int subtitleViewId, int subtitleId) {
		String subtitle = "";
		if (subtitleId >= 0) {
			subtitle = getString(subtitleId).toUpperCase();
		}
		setRowValue(view, titleViewId, titleId, subjectViewId, subject, subtitleViewId, subtitle);
	}
    
    @SuppressLint("DefaultLocale")
	private void setRowValue(View view, int titleViewId, int titleId, int subjectViewId, String subject, int subtitleViewId, String subtitle) {
		
		if (titleViewId >= 0 && titleId >= 0) {
			//Title
			TextView titleView = (TextView) view.findViewById(titleViewId);
			String title = getString(titleId).toUpperCase();
			titleView.setText(title);
		}
		
		if (subjectViewId >= 0) {
			//Subject
			TextView subjectView = (TextView) view.findViewById(subjectViewId);
			subjectView.setText(subject);
		}
		if (subtitleViewId >= 0) {
			//Subtitle
			TextView subTitleView = (TextView) view.findViewById(subtitleViewId);
			subTitleView.setText(subtitle);
		}
    }
    
    /*
     * END
     *
     */

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		//The first row shows the totals, so it shouldn't be clickable
		if (position == 0) {
			return;
		}
		
		long fuelUpId = (Long) v.getTag(R.id.fuelUpIdTag);
		if (fuelUpId < 0) {
			return;
		}
		
		long previousFuelUpId = -1;
		if (position == 1) {
			if (l.getChildCount() > 2) {
				View previous = l.getChildAt(2);
				previousFuelUpId = (Long) previous.getTag(R.id.fuelUpIdTag);
			}
		}
		
		Intent intent = new Intent(this, EditFuelUpActivity.class);
		intent.putExtra(EditFuelUpActivity.VEHICLE_ID, mVehicleId);
		intent.putExtra(EditFuelUpActivity.FUEL_UP_ID, fuelUpId);
		intent.putExtra(EditFuelUpActivity.PREVIOUS_FUEL_UP_ID, previousFuelUpId);
		startActivityForResult(intent, EDIT_FUEL_UP);
	}

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	
        if (resultCode != Activity.RESULT_OK) {
        	return;
        }
        
        if (requestCode == EDIT_FUEL_UP) {
        	mResultCode = RESULT_OK;
        	populateFuelUpList();
        	//TODO: keep list on same row as was selected, so don't let the row go all the way to the top again.
        }
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean itemSelected;
		
		switch (item.getItemId()) {
		case android.R.id.home:
			setResult(mResultCode);
    		finish();
			itemSelected = true;
			break;

		default:
			itemSelected = super.onOptionsItemSelected(item);
			break;
		}
		
		return itemSelected;
	}
	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	
    	switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			setResult(mResultCode);
			finish();
			return true;

		default:
			return super.onKeyDown(keyCode, event);
		}
    }
}
