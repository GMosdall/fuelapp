package com.bithyve.android.fuelapp.vehicles.fuelup;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

class FuelUpAdapter extends ArrayAdapter<FuelUpListItem> {
	
	FuelUpAdapter(Context context) {
		super(context, 0);
	}

	public View getView(int position, View rowView, ViewGroup parent) {
		FuelUpListItem item = getItem(position);
		return item.view;
	}
}
