package com.bithyve.android.fuelapp.preferences;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.bithyve.android.fuelapp.R;


public enum EfficiencyUnit {
	KM_PER_LITRE 		 (0, R.string.Kilometres_per_litre, R.string.Kilometres_per_litre_abb, R.string.Kilometres_per_litre),
	LITRE_PER_HUNDRED_KM (1, R.string.Litres_per_100_kilometres, R.string.Litres_per_100_kilometres_abb, R.string.Litres_per_100_kilometres),
	MILES_PER_GALLON_UK  (2, R.string.Miles_per_gallon, R.string.Miles_per_gallon_abb, R.string.Miles_per_gallon_UK),
	MILES_PER_GALLON_US  (3, R.string.Miles_per_gallon, R.string.Miles_per_gallon_abb, R.string.Miles_per_gallon_US),
	MILES_PER_LITRE 	 (4, R.string.Miles_per_litre, R.string.Miles_per_litre_abb, R.string.Miles_per_litre),
	KM_PER_GALLON_UK 	 (5, R.string.Kilometres_per_gallon, R.string.Kilometres_per_gallon_abb, R.string.Kilometres_per_gallon_UK),
	KM_PER_GALLON_US 	 (6, R.string.Kilometres_per_gallon, R.string.Kilometres_per_gallon_abb, R.string.Kilometres_per_gallon_US);
	
	public static List<EfficiencyUnit> getAll() {
		List<EfficiencyUnit> list = new ArrayList<EfficiencyUnit>();
		list.add(KM_PER_LITRE);
		list.add(LITRE_PER_HUNDRED_KM);
		list.add(MILES_PER_GALLON_UK);
		list.add(MILES_PER_GALLON_US);
		list.add(MILES_PER_LITRE);
		list.add(KM_PER_GALLON_UK);
		list.add(KM_PER_GALLON_US);
		
		return list;
	}
	
	public static EfficiencyUnit newInstance(int id) {
		EfficiencyUnit unit;
		
		switch (id) {
		case 0:
			unit = KM_PER_LITRE;
			break;
			
		case 1:
			unit = LITRE_PER_HUNDRED_KM;
			break;
			
		case 2:
			unit = MILES_PER_GALLON_UK;
			break;
			
		case 3:
			unit = MILES_PER_GALLON_US;
			break;
			
		case 4:
			unit = MILES_PER_LITRE;
			break;
			
		case 5:
			unit = KM_PER_GALLON_UK;
			break;
			
		case 6:
			unit = KM_PER_GALLON_US;
			break;

		default:
			unit = KM_PER_LITRE;
			break;
		}
		
		return unit;
	}
	
	private final int id;
	private final int resId;
	private final int resIdAbb;
	private final int resIdFull;
	private String fullNameWithAbb;
	
	EfficiencyUnit(int id, int resId, int resIdAbb, int resIdFull) {
		this.id = id;
		this.resId = resId;
		this.resIdAbb = resIdAbb;
		this.resIdFull = resIdFull;
	}
	
	public int getId() {
		return id;
	}

	public int getResId() {
		return resId;
	}

	public int getResIdAbb() {
		return resIdAbb;
	}

	public int getResIdFull() {
		return resIdFull;
	}
	
	public String getFullNameWithAbbreviation(Context ctx) {
		if (fullNameWithAbb == null || fullNameWithAbb.length() < 1) {
			String name = ctx.getString(getResIdFull());
	    	String abbreviation = ctx.getString(getResIdAbb());
	    	StringBuilder sb = new StringBuilder(name).append(" ").append("(").append(abbreviation).append(")");
	    	fullNameWithAbb = sb.toString();
		}
    	
    	return fullNameWithAbb;
	}
}
