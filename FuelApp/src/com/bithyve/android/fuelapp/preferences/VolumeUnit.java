package com.bithyve.android.fuelapp.preferences;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.bithyve.android.fuelapp.R;

public enum VolumeUnit {
	LITRES 	   (0, R.string.Litres, R.string.Litres_abb, R.string.Litres),
	GALLONS_UK (1, R.string.Gallons, R.string.Gallons_abb, R.string.Gallons_UK),
	GALLONS_US (2, R.string.Gallons, R.string.Gallons_abb, R.string.Gallons_US);
	
	public static List<VolumeUnit> getAll() {
		List<VolumeUnit> list = new ArrayList<VolumeUnit>();
		list.add(LITRES);
		list.add(GALLONS_UK);
		list.add(GALLONS_US);
		
		return list;
	}
	
	public static VolumeUnit newInstance(int id) {
		VolumeUnit unit;
		
		switch (id) {
		case 0:
			unit = LITRES;
			break;
			
		case 1:
			unit = GALLONS_UK;
			break;
			
		case 2:
			unit = GALLONS_US;
			break;

		default:
			unit = LITRES;
			break;
		}
		
		return unit;
	}

	private final int id;
	private final int resId;
	private final int resIdAbb;
	private final int resIdFull;
	private String fullNameWithAbb;

	private VolumeUnit(int id, int resId, int resIdAbb, int resIdFull) {
		this.id = id;
		this.resId = resId;
		this.resIdAbb = resIdAbb;
		this.resIdFull = resIdFull;
	}
	
	public int getId() {
		return id;
	}

	public int getResId() {
		return resId;
	}

	public int getResIdAbb() {
		return resIdAbb;
	}

	public int getResIdFull() {
		return resIdFull;
	}
	
	public String getFullNameWithAbbreviation(Context ctx) {
		if (fullNameWithAbb == null || fullNameWithAbb.length() < 1) {
			String name = ctx.getString(getResIdFull());
	    	String abbreviation = ctx.getString(getResIdAbb());
	    	StringBuilder sb = new StringBuilder(name).append(" ").append("(").append(abbreviation).append(")");
	    	fullNameWithAbb = sb.toString();
		}
    	
    	return fullNameWithAbb;
	}
}
