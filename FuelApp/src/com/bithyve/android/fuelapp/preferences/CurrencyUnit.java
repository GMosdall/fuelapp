package com.bithyve.android.fuelapp.preferences;

import java.util.Currency;
import java.util.Locale;

import android.content.Context;

import com.bithyve.android.fuelapp.utils.CurrencyUtil;

public class CurrencyUnit {
	private String currencyCode;
	private String currencySymbol;
	private String currencyName;

	public CurrencyUnit(String currencyCode) {
		this.currencyCode = currencyCode;

		Currency currency;
		if (currencyCode != null && currencyCode.length() > 0) {
			currency = Currency.getInstance(currencyCode);
		} else {
			//Fall back to the default currency
			Locale locale = Locale.getDefault();
			currency = Currency.getInstance(locale);
		}
		
		currencySymbol = currency.getSymbol();
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public String getCurrencyName(Context ctx) {
		if (currencyName == null || currencyName.length() == 0) {
			currencyName = CurrencyUtil.getDisplayName(currencyCode, ctx);
		}
		
		return currencyName;
	}
}
