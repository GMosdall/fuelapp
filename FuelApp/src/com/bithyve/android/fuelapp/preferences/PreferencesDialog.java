package com.bithyve.android.fuelapp.preferences;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class PreferencesDialog extends DialogFragment {

	public static final String DIALOG_TITLE = "dialogTitle";
	public static final String CONTENT_LIST = "contentList";
	
	public static PreferencesDialog newInstance(int titleId, String[] contentList) {
		PreferencesDialog dialog = new PreferencesDialog();
		
		Bundle bundle = new Bundle();
		bundle.putInt(DIALOG_TITLE, titleId);
		bundle.putStringArray(CONTENT_LIST, contentList);
		dialog.setArguments(bundle);
		
		return dialog;
	}
	
	public interface DialogResultListener {
		public void onDialogResultSelected(String tag, int which);
	}
	
	
	private DialogResultListener mListener;
	
	public void setResultListener(DialogResultListener listener) {
		mListener = listener;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		Bundle extras = getArguments();
		int titleId = extras.getInt(DIALOG_TITLE);
		String[] currencies = extras.getStringArray(CONTENT_LIST);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    builder.setTitle(titleId)
	           .setItems(currencies, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
		               // The 'which' argument contains the index position
		               // of the selected item
	            	   
	            	   if (mListener != null) {
	            		   String tag = PreferencesDialog.this.getTag();
	            		   mListener.onDialogResultSelected(tag, which);
					}
	           }
	    });
	    return builder.create();
	}

}
