package com.bithyve.android.fuelapp.preferences.rows;

import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.preferences.PreferencesDialog.DialogResultListener;

public abstract class AbstractRow implements DialogResultListener {
	
	protected FragmentManager mFragmentManager;
	protected Context mCtx;
	protected TextView mContentView;

	protected AbstractRow(FragmentManager fragmentManager, Context ctx) {
		this.mFragmentManager = fragmentManager;
		this.mCtx = ctx;
	}
	
	protected View getRowView(LayoutInflater inflater, int titleStringId, String contentString) {
		View row = inflater.inflate(R.layout.preferences_row, null);
    	
		TextView titleView = (TextView) row.findViewById(R.id.titleView);
		titleView.setText(titleStringId);
		
		mContentView = (TextView) row.findViewById(R.id.contentView);
		mContentView.setText(contentString);
		
		return row;
    }
	
	protected abstract OnClickListener getOnClickListener();

}
