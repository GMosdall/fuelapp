package com.bithyve.android.fuelapp.preferences.rows;

import java.util.List;

import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.GlobalPreferences;
import com.bithyve.android.fuelapp.preferences.DistanceUnit;
import com.bithyve.android.fuelapp.preferences.PreferencesDialog;

public class DistanceRow extends AbstractRow {

	private final String DISTANCE_DIALOG_TAG = "distanceDialogTag";
	
	private List<DistanceUnit> mDistanceList;
	private String[] mDistanceNames;

	public DistanceRow(FragmentManager fragmentManager, Context ctx) {
		super(fragmentManager, ctx);
	}
	
	private List<DistanceUnit> getDistanceList() {
		if (mDistanceList == null || mDistanceList.isEmpty()) {
			mDistanceList = DistanceUnit.getAll();
		}
		
		return mDistanceList;
	}
	
	private String[] getDistanceNames() {
		List<DistanceUnit> distanceList = getDistanceList();
		
		if (mDistanceNames == null || mDistanceNames.length < 1) {
			
			mDistanceNames = new String[distanceList.size()];
			for (int i = 0; i < distanceList.size(); i++) {
				DistanceUnit distanceUnit = distanceList.get(i);
				String displayName = mCtx.getString(distanceUnit.getResId());
				mDistanceNames[i] = displayName;
			}
		}
		
		return mDistanceNames;
	}
	
	public View getRowView(LayoutInflater inflater) {
		GlobalPreferences gp = new GlobalPreferences(mCtx);
		DistanceUnit distanceUnit = gp.getDistanceUnit();
		String fullDistanceString = distanceUnit.getFullNameWithAbbreviation(mCtx);
		
		View row = super.getRowView(inflater, R.string.Track_distance_in, fullDistanceString);
		row.setOnClickListener(getOnClickListener());
		
		return row;
	}

	@Override
	protected OnClickListener getOnClickListener() {
		OnClickListener onClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String[] distanceNames = getDistanceNames();
			    
			    PreferencesDialog currenciesDialog = PreferencesDialog.newInstance(R.string.Track_distance_in, distanceNames);
				currenciesDialog.setResultListener(DistanceRow.this);
				currenciesDialog.show(mFragmentManager, DISTANCE_DIALOG_TAG);
			}
		};
		
		return onClickListener;
	}

	@Override
	public void onDialogResultSelected(String tag, int which) {
    	GlobalPreferences gp = new GlobalPreferences(mCtx);
		List<DistanceUnit> distanceList = getDistanceList();
		
		if (distanceList != null && which < distanceList.size()) {
			DistanceUnit distanceUnit = distanceList.get(which);
			gp.putDistanceUnit(distanceUnit);
			gp.apply();
			
			String fullDistanceName = distanceUnit.getFullNameWithAbbreviation(mCtx);
			mContentView.setText(fullDistanceName);
		}
	}
}
