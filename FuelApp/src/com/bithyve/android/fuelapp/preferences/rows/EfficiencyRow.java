package com.bithyve.android.fuelapp.preferences.rows;

import java.util.List;

import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.GlobalPreferences;
import com.bithyve.android.fuelapp.preferences.EfficiencyUnit;
import com.bithyve.android.fuelapp.preferences.PreferencesDialog;

public class EfficiencyRow extends AbstractRow {

	private final String EFFICIENCY_DIALOG_TAG = "efficiencyDialogTag";
	
	private List<EfficiencyUnit> mEfficiencyList;
	private String[] mEfficiencyNames;

	public EfficiencyRow(FragmentManager fragmentManager, Context ctx) {
		super(fragmentManager, ctx);
	}
	
	private List<EfficiencyUnit> getEfficiencyList() {
		if (mEfficiencyList == null || mEfficiencyList.isEmpty()) {
			mEfficiencyList = EfficiencyUnit.getAll();
		}
		
		return mEfficiencyList;
	}
	
	private String[] getEfficiencyNames() {
		List<EfficiencyUnit> efficiencyList = getEfficiencyList();
		
		if (mEfficiencyNames == null || mEfficiencyNames.length < 1) {
			
			mEfficiencyNames = new String[efficiencyList.size()];
			for (int i = 0; i < efficiencyList.size(); i++) {
				EfficiencyUnit efficiencyUnit = efficiencyList.get(i);
				String displayName = efficiencyUnit.getFullNameWithAbbreviation(mCtx);
				mEfficiencyNames[i] = displayName;
			}
		}
		
		return mEfficiencyNames;
	}
	
	public View getRowView(LayoutInflater inflater) {
    	GlobalPreferences gp = new GlobalPreferences(mCtx);
		EfficiencyUnit efficiencyUnit = gp.getEfficiencyUnit();
		String fullEfficiencyString = efficiencyUnit.getFullNameWithAbbreviation(mCtx);
		
		View row = super.getRowView(inflater, R.string.Display_efficiency_in, fullEfficiencyString);
		row.setOnClickListener(getOnClickListener());
		
		return row;
	}

	@Override
	protected OnClickListener getOnClickListener() {
		OnClickListener onClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String[] efficiencyNames = getEfficiencyNames();
			    
			    PreferencesDialog currenciesDialog = PreferencesDialog.newInstance(R.string.Display_efficiency_in, efficiencyNames);
				currenciesDialog.setResultListener(EfficiencyRow.this);
				currenciesDialog.show(mFragmentManager, EFFICIENCY_DIALOG_TAG);
			}
		};
		
		return onClickListener;
	}

	@Override
	public void onDialogResultSelected(String tag, int which) {
    	GlobalPreferences gp = new GlobalPreferences(mCtx);
		List<EfficiencyUnit> efficiencyList = getEfficiencyList();

    	if (efficiencyList != null && which < efficiencyList.size()) {
			EfficiencyUnit efficiencyUnit = mEfficiencyList.get(which);
			gp.putEfficiencyUnit(efficiencyUnit);
			gp.apply();
			
			String fullEfficiencyName = efficiencyUnit.getFullNameWithAbbreviation(mCtx);
			mContentView.setText(fullEfficiencyName);
		}
	}
}
