package com.bithyve.android.fuelapp.preferences.rows;

import java.util.List;

import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.GlobalPreferences;
import com.bithyve.android.fuelapp.preferences.PreferencesDialog;
import com.bithyve.android.fuelapp.preferences.VolumeUnit;

public class VolumeRow extends AbstractRow {

	private final String VOLUME_DIALOG_TAG = "volumeDialogTag";
	
	private List<VolumeUnit> mVolumeList;
	private String[] mVolumeNames;

	public VolumeRow(FragmentManager fragmentManager, Context ctx) {
		super(fragmentManager, ctx);
	}
	
	private List<VolumeUnit> getVolumeList() {
		
		if (mVolumeList == null || mVolumeList.isEmpty()) {
			mVolumeList = VolumeUnit.getAll();
		}
		
		return mVolumeList;
	}
	
	private String[] getVolumeNames() {
		List<VolumeUnit> volumeList = getVolumeList();
		
		if (mVolumeNames == null || mVolumeNames.length < 1) {
			
			mVolumeNames = new String[volumeList.size()];
			for (int i = 0; i < volumeList.size(); i++) {
				VolumeUnit volumeUnit = volumeList.get(i);
				String displayName = mCtx.getString(volumeUnit.getResIdFull());
				mVolumeNames[i] = displayName;
			}
		}
		
		return mVolumeNames;
	}
	
	public View getRowView(LayoutInflater inflater) {
		GlobalPreferences gp = new GlobalPreferences(mCtx);
		VolumeUnit volumeUnit = gp.getVolumeUnit();
    	String fullVolumeString = volumeUnit.getFullNameWithAbbreviation(mCtx);
		
		View row = super.getRowView(inflater, R.string.Track_fuel_in, fullVolumeString);
		row.setOnClickListener(getOnClickListener());
		
		return row;
	}

	@Override
	protected OnClickListener getOnClickListener() {
		OnClickListener onClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String[] volumeNames = getVolumeNames();
			    
			    PreferencesDialog currenciesDialog = PreferencesDialog.newInstance(R.string.Track_fuel_in, volumeNames);
				currenciesDialog.setResultListener(VolumeRow.this);
				currenciesDialog.show(mFragmentManager, VOLUME_DIALOG_TAG);
			}
		};
		
		return onClickListener;
	}

	@Override
	public void onDialogResultSelected(String tag, int which) {
    	GlobalPreferences gp = new GlobalPreferences(mCtx);
    	List<VolumeUnit> volumeList = getVolumeList();
    	
		if (volumeList != null && which < volumeList.size()) {
			VolumeUnit volumeUnit = volumeList.get(which);
			gp.putVolumeUnit(volumeUnit);
			gp.apply();
			
			String fullVolumeName = volumeUnit.getFullNameWithAbbreviation(mCtx);
			mContentView.setText(fullVolumeName);
		}
	}
}
