package com.bithyve.android.fuelapp.preferences.rows;

import java.util.List;

import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.GlobalPreferences;
import com.bithyve.android.fuelapp.preferences.PreferencesDialog;
import com.bithyve.android.fuelapp.preferences.TrackMeterUnit;

public class TrackMeterRow extends AbstractRow {
	
	private final String TRACK_METER_DIALOG_TAG = "trackMeterDialogTag";
	
	private List<TrackMeterUnit> mTrackMeterList;
	private String[] mTrackMeterNames;

	public TrackMeterRow(FragmentManager fragmentManager, Context ctx) {
		super(fragmentManager, ctx);
	}
	
	private List<TrackMeterUnit> getTrackMeterList() {
		if (mTrackMeterList == null || mTrackMeterList.isEmpty()) {
			mTrackMeterList = TrackMeterUnit.getAll();
		}
		
		return mTrackMeterList;
	}
	
	private String[] getTrackMeterNames() {
		List<TrackMeterUnit> trackMeterList = getTrackMeterList();
		
		if (mTrackMeterNames == null || mTrackMeterNames.length < 1) {
			
			mTrackMeterNames = new String[trackMeterList.size()];
			for (int i = 0; i < trackMeterList.size(); i++) {
				TrackMeterUnit trackMeterUnit = trackMeterList.get(i);
				String displayName = mCtx.getString(trackMeterUnit.getResId());
				mTrackMeterNames[i] = displayName;
			}
		}
		
		return mTrackMeterNames;
	}
	
	public View getRowView(LayoutInflater inflater) {
    	GlobalPreferences gp = new GlobalPreferences(mCtx);
		TrackMeterUnit trackMeterUnit = gp.getTrackMeterUnit();
		String trackMeterString = trackMeterUnit.getName(mCtx);
		
		View row = super.getRowView(inflater, R.string.Track_distance_with, trackMeterString);
		row.setOnClickListener(getOnClickListener());
		
		return row;
	}

	@Override
	protected OnClickListener getOnClickListener() {
		OnClickListener onClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String[] trackMeterNames = getTrackMeterNames();
			    
			    PreferencesDialog currenciesDialog = PreferencesDialog.newInstance(R.string.Track_distance_with, trackMeterNames);
				currenciesDialog.setResultListener(TrackMeterRow.this);
				currenciesDialog.show(mFragmentManager, TRACK_METER_DIALOG_TAG);
			}
		};
		
		return onClickListener;
	}

	@Override
	public void onDialogResultSelected(String tag, int which) {
    	GlobalPreferences gp = new GlobalPreferences(mCtx);
    	List<TrackMeterUnit> trackMeterList = getTrackMeterList();
		
		if (trackMeterList != null && which < trackMeterList.size()) {
			TrackMeterUnit trackMeterUnit = trackMeterList.get(which);
			gp.putTrackMeterUnit(trackMeterUnit);
			gp.apply();
			
			String fullTrackMeterName = trackMeterUnit.getName(mCtx);
			mContentView.setText(fullTrackMeterName);
		}
	}
}
