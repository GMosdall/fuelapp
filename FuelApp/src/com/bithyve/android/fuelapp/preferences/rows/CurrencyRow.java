package com.bithyve.android.fuelapp.preferences.rows;

import java.util.Currency;
import java.util.Locale;
import java.util.TreeMap;

import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.data.GlobalPreferences;
import com.bithyve.android.fuelapp.preferences.CurrencyUnit;
import com.bithyve.android.fuelapp.preferences.PreferencesDialog;
import com.bithyve.android.fuelapp.utils.CurrencyUtil;

public class CurrencyRow extends AbstractRow {

	private final String CURRENCY_DIALOG_TAG = "currencyDialogTag";
	
	private TreeMap<String, String> mCurrencyMap;
	private String[] mCurrencyNames;
	
	public CurrencyRow(FragmentManager fragmentManager, Context ctx) {
		super(fragmentManager, ctx);
	}

	private TreeMap<String, String> getCurrencyMap() {
		
		if (mCurrencyMap == null || mCurrencyMap.isEmpty()) {
			mCurrencyMap = new TreeMap<String, String>();
		    Locale[] locales = Locale.getAvailableLocales();

		    for(Locale locale : locales) {
				try {
					Currency currency = Currency.getInstance(locale);
					String currencyCode = currency.getCurrencyCode();
					if (!mCurrencyMap.containsValue(currencyCode)) {
						String displayName = CurrencyUtil.getDisplayNameWithSymbol(currency, mCtx);

						//Input is being sorted based on the keys not the values
						mCurrencyMap.put(displayName, currencyCode);
//						currencies.put(currencyCode, displayName);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }
		}
		
		return mCurrencyMap;
	}

	private String[] getCurrencyNames() {
		TreeMap<String, String> currencyMap = getCurrencyMap();
		
		if (mCurrencyNames == null || mCurrencyNames.length == 0) {
        	mCurrencyNames = new String[currencyMap.size()];
        	currencyMap.keySet().toArray(mCurrencyNames);
		}
		
		return mCurrencyNames;
	}
	
	public View getRowView(LayoutInflater inflater) {
    	GlobalPreferences gp = new GlobalPreferences(mCtx);
		CurrencyUnit currency = gp.getCurrencyUnit();
		String fullCurrencyString = CurrencyUtil.getDisplayNameWithSymbol(currency.getCurrencyCode(), mCtx);
		
		View row = super.getRowView(inflater, R.string.Currency, fullCurrencyString);
		row.setOnClickListener(getOnClickListener());
		
		return row;
	}

	@Override
	protected OnClickListener getOnClickListener() {
		OnClickListener onClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String[] currencyNames = getCurrencyNames();
			    
			    PreferencesDialog currenciesDialog = PreferencesDialog.newInstance(R.string.Select_your_currency, currencyNames);
				currenciesDialog.setResultListener(CurrencyRow.this);
				currenciesDialog.show(mFragmentManager, CURRENCY_DIALOG_TAG);
			}
		};
		
		return onClickListener;
	}

	@Override
	public void onDialogResultSelected(String tag, int which) {
    	GlobalPreferences gp = new GlobalPreferences(mCtx);
		String[] currencyNames = getCurrencyNames();
		
		if (currencyNames != null && which < currencyNames.length) {
			String currencyName = currencyNames[which];

			TreeMap<String, String> currencyMap = getCurrencyMap();
			if (currencyMap != null && currencyMap.containsKey(currencyName)) {
				String currencyCode = currencyMap.get(currencyName);
				CurrencyUnit currency = new CurrencyUnit(currencyCode);
		    	gp.putCurrencyUnit(currency);
		    	gp.apply();
		    	
		    	mContentView.setText(currencyName);
			}
		}
	}
}
