package com.bithyve.android.fuelapp.preferences;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.bithyve.android.fuelapp.R;

public enum TrackMeterUnit {
	TRIPMETER (0, R.string.Trip_meter, R.string.Distance_between_fuel_ups),
	ODOMETER  (1, R.string.Odometer, R.string.Total_distance_of_vehicle);
	
	public static List<TrackMeterUnit> getAll() {
		List<TrackMeterUnit> list = new ArrayList<TrackMeterUnit>();
		list.add(TRIPMETER);
		list.add(ODOMETER);
		
		return list;
	}
	
	public static TrackMeterUnit newInstance(int id) {
		TrackMeterUnit unit;
		
		switch (id) {
		case 0:
			unit = TRIPMETER;
			break;
			
		case 1:
			unit = ODOMETER;
			break;

		default:
			unit = TRIPMETER;
			break;
		}
		
		return unit;
	}

	private final int id;
	private final int resId;
	private final int resIdDescription;
	private String name;

	private TrackMeterUnit(int id, int resId, int resIdDescription) {
		this.id = id;
		this.resId = resId;
		this.resIdDescription = resIdDescription;
	}
	
	public int getId() {
		return id;
	}

	public int getResId() {
		return resId;
	}

	public int getResIdDescription() {
		return resIdDescription;
	}
	
	public String getName(Context ctx) {
		if (name == null || name.length() < 1) {
			name = ctx.getString(getResId());
		}
    	
    	return name;
	}
}
