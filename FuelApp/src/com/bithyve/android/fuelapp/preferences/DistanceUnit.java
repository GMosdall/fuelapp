package com.bithyve.android.fuelapp.preferences;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.bithyve.android.fuelapp.R;

public enum DistanceUnit {
	KILOMETRES (0, R.string.Kilometres, R.string.Kilometres_abb),
	MILES	   (1, R.string.Miles, R.string.Miles_abb);
	
	public static List<DistanceUnit> getAll() {
		List<DistanceUnit> list = new ArrayList<DistanceUnit>();
		list.add(KILOMETRES);
		list.add(MILES);
		
		return list;
	}
	
	public static DistanceUnit newInstance(int id) {
		DistanceUnit unit;
		
		switch (id) {
		case 0:
			unit = KILOMETRES;
			break;
			
		case 1:
			unit = MILES;
			break;

		default:
			unit = KILOMETRES;
			break;
		}
		
		return unit;
	}

	private final int id;
	private final int resId;
	private final int resIdAbb;
	private String fullNameWithAbb;

	private DistanceUnit(int id, int resId, int resIdAbb) {
		this.id = id;
		this.resId = resId;
		this.resIdAbb = resIdAbb;
	}
	
	public int getId() {
		return id;
	}

	public int getResId() {
		return resId;
	}

	public int getResIdAbb() {
		return resIdAbb;
	}
	
	public String getFullNameWithAbbreviation(Context ctx) {
		if (fullNameWithAbb == null || fullNameWithAbb.length() < 1) {
			String name = ctx.getString(getResId());
	    	String abbreviation = ctx.getString(getResIdAbb());
	    	StringBuilder sb = new StringBuilder(name).append(" ").append("(").append(abbreviation).append(")");
	    	fullNameWithAbb = sb.toString();
		}
    	
    	return fullNameWithAbb;
	}
}
