package com.bithyve.android.fuelapp.preferences;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bithyve.android.fuelapp.R;
import com.bithyve.android.fuelapp.preferences.rows.CurrencyRow;
import com.bithyve.android.fuelapp.preferences.rows.DistanceRow;
import com.bithyve.android.fuelapp.preferences.rows.EfficiencyRow;
import com.bithyve.android.fuelapp.preferences.rows.TrackMeterRow;
import com.bithyve.android.fuelapp.preferences.rows.VolumeRow;

public class PreferencesFragment extends Fragment {
	
	private ActionBar mActionBar;
	private Context mCtx;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//Set the actionbar background
		mActionBar = getActivity().getActionBar();
		mActionBar.setTitle(R.string.Preferences);
		Drawable actionBarBg = getResources().getDrawable(R.drawable.ab_background_blue);
        mActionBar.setBackgroundDrawable(actionBarBg);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
        	actionBarBg.setCallback(mDrawableCallback);
        }
        
        mCtx = getActivity();

        View view = getFragmentView(inflater);
		
		return view;
	}

	/*
	 * Begin of FadingActionBar shizzle
	 */
    private Drawable.Callback mDrawableCallback = new Drawable.Callback() {
        @Override
        public void invalidateDrawable(Drawable who) {
            mActionBar.setBackgroundDrawable(who);
        }

        @Override
        public void scheduleDrawable(Drawable who, Runnable what, long when) {
        }

        @Override
        public void unscheduleDrawable(Drawable who, Runnable what) {
        }
    };
    /*
     * End of FadingActionBar shizzle
     */
    
    private View getFragmentView(LayoutInflater inflater) {
		View preferencesFragment = inflater.inflate(R.layout.preferences_fragment, null);
    	ViewGroup regionalPrefContainer = (ViewGroup) preferencesFragment.findViewById(R.id.regionalPrefContainer);
    	
    	//Currency
    	CurrencyRow currencyRow = new CurrencyRow(getFragmentManager(), mCtx);
    	View currencyView = currencyRow.getRowView(inflater);
    	regionalPrefContainer.addView(currencyView);
    	
    	//Volume unit
    	VolumeRow volumeRow = new VolumeRow(getFragmentManager(), mCtx);
    	View volumehUnitRow = volumeRow.getRowView(inflater);
    	regionalPrefContainer.addView(volumehUnitRow);

    	//Global vehicle prefences
    	ViewGroup vehiclePrefContainer = (ViewGroup) preferencesFragment.findViewById(R.id.vehiclePrefContainer);
    	
    	//Track meter unit
    	TrackMeterRow trackMeterRow = new TrackMeterRow(getFragmentManager(), mCtx);
    	View trackMeterUnitRow = trackMeterRow.getRowView(inflater);
    	vehiclePrefContainer.addView(trackMeterUnitRow);
    	
    	//Distance unit
    	DistanceRow distanceRow = new DistanceRow(getFragmentManager(), mCtx);
    	View distanceUnitRow = distanceRow.getRowView(inflater);
    	vehiclePrefContainer.addView(distanceUnitRow);
    	
    	//Efficiency notation
    	EfficiencyRow efficiencyRow = new EfficiencyRow(getFragmentManager(), mCtx);
    	View efficiencyNotationRow = efficiencyRow.getRowView(inflater);
    	vehiclePrefContainer.addView(efficiencyNotationRow);
    	
    	return preferencesFragment;
    }
}
